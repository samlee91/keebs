EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A2 23386 16535
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text GLabel 3600 12600 0    50   Input ~ 0
row0
Text GLabel 3600 12400 0    50   Input ~ 0
row2
Text GLabel 3600 12300 0    50   Input ~ 0
row3
Text GLabel 3600 11600 0    50   Input ~ 0
col0
Text GLabel 5750 12100 2    50   Input ~ 0
col3
Text GLabel 5750 12200 2    50   Input ~ 0
col4
Text GLabel 5750 12300 2    50   Input ~ 0
col5
Text GLabel 5750 12400 2    50   Input ~ 0
col6
Text GLabel 5750 12900 2    50   Input ~ 0
col8
Text GLabel 1650 3050 0    50   Input ~ 0
row0
Wire Wire Line
	1650 3050 1900 3050
Text GLabel 2550 2300 0    50   Input ~ 0
col0
Wire Wire Line
	2550 2300 2550 2650
$Comp
L keyboard_parts:KEYSW K_Esc1
U 1 1 00000001
P 2250 2650
F 0 "K_Esc1" H 2250 2883 60  0000 C CNN
F 1 "KEYSW" H 2250 2550 60  0001 C CNN
F 2 "keyswitches:Kailh_socket_MX_1u" H 2250 2650 60  0001 C CNN
F 3 "" H 2250 2650 60  0000 C CNN
	1    2250 2650
	1    0    0    -1  
$EndComp
$Comp
L Device:D D_Esc1
U 1 1 00000000
P 1900 2900
F 0 "D_Esc1" V 1946 2821 50  0000 R CNN
F 1 "D" V 1845 2821 50  0000 R CNN
F 2 "keyboard_parts:D_SOD123_axial" H 1900 2900 50  0001 C CNN
F 3 "~" H 1900 2900 50  0001 C CNN
F 4 "C81598" H 1900 2900 50  0001 C CNN "JLC PCB Part Number"
	1    1900 2900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1950 2650 1900 2650
Wire Wire Line
	1900 2650 1900 2750
Connection ~ 2550 2650
Connection ~ 1900 3050
Text GLabel 3550 2300 0    50   Input ~ 0
col1
Text GLabel 4550 2300 0    50   Input ~ 0
col2
Wire Wire Line
	4550 2300 4550 2650
$Comp
L keyboard_parts:KEYSW K_F1
U 1 1 00000011
P 4250 2650
F 0 "K_F1" H 4250 2883 60  0000 C CNN
F 1 "KEYSW" H 4250 2550 60  0001 C CNN
F 2 "keyswitches:Kailh_socket_MX_1u" H 4250 2650 60  0001 C CNN
F 3 "" H 4250 2650 60  0000 C CNN
	1    4250 2650
	1    0    0    -1  
$EndComp
$Comp
L Device:D D_F1
U 1 1 00000010
P 3900 2900
F 0 "D_F1" V 3946 2821 50  0000 R CNN
F 1 "D" V 3845 2821 50  0000 R CNN
F 2 "keyboard_parts:D_SOD123_axial" H 3900 2900 50  0001 C CNN
F 3 "~" H 3900 2900 50  0001 C CNN
F 4 "C81598" H 3900 2900 50  0001 C CNN "JLC PCB Part Number"
	1    3900 2900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3950 2650 3900 2650
Wire Wire Line
	3900 2650 3900 2750
Connection ~ 4550 2650
Connection ~ 3900 3050
Text GLabel 5550 2300 0    50   Input ~ 0
col3
Wire Wire Line
	5550 2300 5550 2650
$Comp
L keyboard_parts:KEYSW K_F2
U 1 1 00000021
P 5250 2650
F 0 "K_F2" H 5250 2883 60  0000 C CNN
F 1 "KEYSW" H 5250 2550 60  0001 C CNN
F 2 "keyswitches:Kailh_socket_MX_1u" H 5250 2650 60  0001 C CNN
F 3 "" H 5250 2650 60  0000 C CNN
	1    5250 2650
	1    0    0    -1  
$EndComp
$Comp
L Device:D D_F2
U 1 1 00000020
P 4900 2900
F 0 "D_F2" V 4946 2821 50  0000 R CNN
F 1 "D" V 4845 2821 50  0000 R CNN
F 2 "keyboard_parts:D_SOD123_axial" H 4900 2900 50  0001 C CNN
F 3 "~" H 4900 2900 50  0001 C CNN
F 4 "C81598" H 4900 2900 50  0001 C CNN "JLC PCB Part Number"
	1    4900 2900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4950 2650 4900 2650
Wire Wire Line
	4900 2650 4900 2750
Connection ~ 5550 2650
Connection ~ 4900 3050
Text GLabel 6550 2300 0    50   Input ~ 0
col4
Wire Wire Line
	6550 2300 6550 2650
$Comp
L keyboard_parts:KEYSW K_F3
U 1 1 00000031
P 6250 2650
F 0 "K_F3" H 6250 2883 60  0000 C CNN
F 1 "KEYSW" H 6250 2550 60  0001 C CNN
F 2 "keyswitches:Kailh_socket_MX_1u" H 6250 2650 60  0001 C CNN
F 3 "" H 6250 2650 60  0000 C CNN
	1    6250 2650
	1    0    0    -1  
$EndComp
$Comp
L Device:D D_F3
U 1 1 00000030
P 5900 2900
F 0 "D_F3" V 5946 2821 50  0000 R CNN
F 1 "D" V 5845 2821 50  0000 R CNN
F 2 "keyboard_parts:D_SOD123_axial" H 5900 2900 50  0001 C CNN
F 3 "~" H 5900 2900 50  0001 C CNN
F 4 "C81598" H 5900 2900 50  0001 C CNN "JLC PCB Part Number"
	1    5900 2900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5950 2650 5900 2650
Wire Wire Line
	5900 2650 5900 2750
Connection ~ 6550 2650
Connection ~ 5900 3050
Text GLabel 7550 2300 0    50   Input ~ 0
col5
Wire Wire Line
	7550 2300 7550 2650
$Comp
L keyboard_parts:KEYSW K_F4
U 1 1 00000041
P 7250 2650
F 0 "K_F4" H 7250 2883 60  0000 C CNN
F 1 "KEYSW" H 7250 2550 60  0001 C CNN
F 2 "keyswitches:Kailh_socket_MX_1u" H 7250 2650 60  0001 C CNN
F 3 "" H 7250 2650 60  0000 C CNN
	1    7250 2650
	1    0    0    -1  
$EndComp
$Comp
L Device:D D_F4
U 1 1 00000040
P 6900 2900
F 0 "D_F4" V 6946 2821 50  0000 R CNN
F 1 "D" V 6845 2821 50  0000 R CNN
F 2 "keyboard_parts:D_SOD123_axial" H 6900 2900 50  0001 C CNN
F 3 "~" H 6900 2900 50  0001 C CNN
F 4 "C81598" H 6900 2900 50  0001 C CNN "JLC PCB Part Number"
	1    6900 2900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6950 2650 6900 2650
Wire Wire Line
	6900 2650 6900 2750
Connection ~ 7550 2650
Connection ~ 6900 3050
Text GLabel 8550 2300 0    50   Input ~ 0
col6
Text GLabel 9550 2300 0    50   Input ~ 0
col7
Wire Wire Line
	9550 2300 9550 2650
$Comp
L keyboard_parts:KEYSW K_F5
U 1 1 00000051
P 9250 2650
F 0 "K_F5" H 9250 2883 60  0000 C CNN
F 1 "KEYSW" H 9250 2550 60  0001 C CNN
F 2 "keyswitches:Kailh_socket_MX_1u" H 9250 2650 60  0001 C CNN
F 3 "" H 9250 2650 60  0000 C CNN
	1    9250 2650
	1    0    0    -1  
$EndComp
$Comp
L Device:D D_F5
U 1 1 00000050
P 8900 2900
F 0 "D_F5" V 8946 2821 50  0000 R CNN
F 1 "D" V 8845 2821 50  0000 R CNN
F 2 "keyboard_parts:D_SOD123_axial" H 8900 2900 50  0001 C CNN
F 3 "~" H 8900 2900 50  0001 C CNN
F 4 "C81598" H 8900 2900 50  0001 C CNN "JLC PCB Part Number"
	1    8900 2900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8950 2650 8900 2650
Wire Wire Line
	8900 2650 8900 2750
Connection ~ 9550 2650
Connection ~ 8900 3050
Text GLabel 10550 2300 0    50   Input ~ 0
col8
Wire Wire Line
	10550 2300 10550 2650
$Comp
L keyboard_parts:KEYSW K_F6
U 1 1 00000061
P 10250 2650
F 0 "K_F6" H 10250 2883 60  0000 C CNN
F 1 "KEYSW" H 10250 2550 60  0001 C CNN
F 2 "keyswitches:Kailh_socket_MX_1u" H 10250 2650 60  0001 C CNN
F 3 "" H 10250 2650 60  0000 C CNN
	1    10250 2650
	1    0    0    -1  
$EndComp
$Comp
L Device:D D_F6
U 1 1 00000060
P 9900 2900
F 0 "D_F6" V 9946 2821 50  0000 R CNN
F 1 "D" V 9845 2821 50  0000 R CNN
F 2 "keyboard_parts:D_SOD123_axial" H 9900 2900 50  0001 C CNN
F 3 "~" H 9900 2900 50  0001 C CNN
F 4 "C81598" H 9900 2900 50  0001 C CNN "JLC PCB Part Number"
	1    9900 2900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	9950 2650 9900 2650
Wire Wire Line
	9900 2650 9900 2750
Connection ~ 10550 2650
Connection ~ 9900 3050
Text GLabel 11550 2300 0    50   Input ~ 0
col9
Wire Wire Line
	11550 2300 11550 2650
$Comp
L keyboard_parts:KEYSW K_F7
U 1 1 00000071
P 11250 2650
F 0 "K_F7" H 11250 2883 60  0000 C CNN
F 1 "KEYSW" H 11250 2550 60  0001 C CNN
F 2 "keyswitches:Kailh_socket_MX_1u" H 11250 2650 60  0001 C CNN
F 3 "" H 11250 2650 60  0000 C CNN
	1    11250 2650
	1    0    0    -1  
$EndComp
$Comp
L Device:D D_F7
U 1 1 00000070
P 10900 2900
F 0 "D_F7" V 10946 2821 50  0000 R CNN
F 1 "D" V 10845 2821 50  0000 R CNN
F 2 "keyboard_parts:D_SOD123_axial" H 10900 2900 50  0001 C CNN
F 3 "~" H 10900 2900 50  0001 C CNN
F 4 "C81598" H 10900 2900 50  0001 C CNN "JLC PCB Part Number"
	1    10900 2900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	10950 2650 10900 2650
Wire Wire Line
	10900 2650 10900 2750
Connection ~ 11550 2650
Connection ~ 10900 3050
Text GLabel 12550 2300 0    50   Input ~ 0
col10
Wire Wire Line
	12550 2300 12550 2650
$Comp
L keyboard_parts:KEYSW K_F8
U 1 1 00000081
P 12250 2650
F 0 "K_F8" H 12250 2883 60  0000 C CNN
F 1 "KEYSW" H 12250 2550 60  0001 C CNN
F 2 "keyswitches:Kailh_socket_MX_1u" H 12250 2650 60  0001 C CNN
F 3 "" H 12250 2650 60  0000 C CNN
	1    12250 2650
	1    0    0    -1  
$EndComp
$Comp
L Device:D D_F8
U 1 1 00000080
P 11900 2900
F 0 "D_F8" V 11946 2821 50  0000 R CNN
F 1 "D" V 11845 2821 50  0000 R CNN
F 2 "keyboard_parts:D_SOD123_axial" H 11900 2900 50  0001 C CNN
F 3 "~" H 11900 2900 50  0001 C CNN
F 4 "C81598" H 11900 2900 50  0001 C CNN "JLC PCB Part Number"
	1    11900 2900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	11950 2650 11900 2650
Wire Wire Line
	11900 2650 11900 2750
Connection ~ 12550 2650
Connection ~ 11900 3050
Text GLabel 13550 2300 0    50   Input ~ 0
col11
Wire Wire Line
	13550 2300 13550 2650
$Comp
L keyboard_parts:KEYSW K_F9
U 1 1 00000091
P 13250 2650
F 0 "K_F9" H 13250 2883 60  0000 C CNN
F 1 "KEYSW" H 13250 2550 60  0001 C CNN
F 2 "keyswitches:Kailh_socket_MX_1u" H 13250 2650 60  0001 C CNN
F 3 "" H 13250 2650 60  0000 C CNN
	1    13250 2650
	1    0    0    -1  
$EndComp
$Comp
L Device:D D_F9
U 1 1 00000090
P 12900 2900
F 0 "D_F9" V 12946 2821 50  0000 R CNN
F 1 "D" V 12845 2821 50  0000 R CNN
F 2 "keyboard_parts:D_SOD123_axial" H 12900 2900 50  0001 C CNN
F 3 "~" H 12900 2900 50  0001 C CNN
F 4 "C81598" H 12900 2900 50  0001 C CNN "JLC PCB Part Number"
	1    12900 2900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	12950 2650 12900 2650
Wire Wire Line
	12900 2650 12900 2750
Connection ~ 13550 2650
Connection ~ 12900 3050
Text GLabel 14550 2300 0    50   Input ~ 0
col12
Wire Wire Line
	14550 2300 14550 2650
$Comp
L keyboard_parts:KEYSW K_F10
U 1 1 000000A1
P 14250 2650
F 0 "K_F10" H 14250 2883 60  0000 C CNN
F 1 "KEYSW" H 14250 2550 60  0001 C CNN
F 2 "keyswitches:Kailh_socket_MX_1u" H 14250 2650 60  0001 C CNN
F 3 "" H 14250 2650 60  0000 C CNN
	1    14250 2650
	1    0    0    -1  
$EndComp
$Comp
L Device:D D_F10
U 1 1 000000A0
P 13900 2900
F 0 "D_F10" V 13946 2821 50  0000 R CNN
F 1 "D" V 13845 2821 50  0000 R CNN
F 2 "keyboard_parts:D_SOD123_axial" H 13900 2900 50  0001 C CNN
F 3 "~" H 13900 2900 50  0001 C CNN
F 4 "C81598" H 13900 2900 50  0001 C CNN "JLC PCB Part Number"
	1    13900 2900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	13950 2650 13900 2650
Wire Wire Line
	13900 2650 13900 2750
Connection ~ 14550 2650
Connection ~ 13900 3050
Text GLabel 15550 2300 0    50   Input ~ 0
col13
Wire Wire Line
	15550 2300 15550 2650
$Comp
L keyboard_parts:KEYSW K_F11
U 1 1 000000B1
P 15250 2650
F 0 "K_F11" H 15250 2883 60  0000 C CNN
F 1 "KEYSW" H 15250 2550 60  0001 C CNN
F 2 "keyswitches:Kailh_socket_MX_1u" H 15250 2650 60  0001 C CNN
F 3 "" H 15250 2650 60  0000 C CNN
	1    15250 2650
	1    0    0    -1  
$EndComp
$Comp
L Device:D D_F11
U 1 1 000000B0
P 14900 2900
F 0 "D_F11" V 14946 2821 50  0000 R CNN
F 1 "D" V 14845 2821 50  0000 R CNN
F 2 "keyboard_parts:D_SOD123_axial" H 14900 2900 50  0001 C CNN
F 3 "~" H 14900 2900 50  0001 C CNN
F 4 "C81598" H 14900 2900 50  0001 C CNN "JLC PCB Part Number"
	1    14900 2900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	14950 2650 14900 2650
Wire Wire Line
	14900 2650 14900 2750
Connection ~ 15550 2650
Connection ~ 14900 3050
Text GLabel 16550 2300 0    50   Input ~ 0
col14
Wire Wire Line
	16550 2300 16550 2650
$Comp
L keyboard_parts:KEYSW K_F12
U 1 1 000000C1
P 16250 2650
F 0 "K_F12" H 16250 2883 60  0000 C CNN
F 1 "KEYSW" H 16250 2550 60  0001 C CNN
F 2 "keyswitches:Kailh_socket_MX_1u" H 16250 2650 60  0001 C CNN
F 3 "" H 16250 2650 60  0000 C CNN
	1    16250 2650
	1    0    0    -1  
$EndComp
$Comp
L Device:D D_F12
U 1 1 000000C0
P 15900 2900
F 0 "D_F12" V 15946 2821 50  0000 R CNN
F 1 "D" V 15845 2821 50  0000 R CNN
F 2 "keyboard_parts:D_SOD123_axial" H 15900 2900 50  0001 C CNN
F 3 "~" H 15900 2900 50  0001 C CNN
F 4 "C81598" H 15900 2900 50  0001 C CNN "JLC PCB Part Number"
	1    15900 2900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	15950 2650 15900 2650
Wire Wire Line
	15900 2650 15900 2750
Connection ~ 16550 2650
Connection ~ 15900 3050
Text GLabel 17550 2300 0    50   Input ~ 0
col15
Wire Wire Line
	17550 2300 17550 2650
$Comp
L keyboard_parts:KEYSW K_7_pad1
U 1 1 000000D1
P 17250 2650
F 0 "K_7_pad1" H 17250 2883 60  0000 C CNN
F 1 "KEYSW" H 17250 2550 60  0001 C CNN
F 2 "keyswitches:Kailh_socket_MX_1u" H 17250 2650 60  0001 C CNN
F 3 "" H 17250 2650 60  0000 C CNN
	1    17250 2650
	1    0    0    -1  
$EndComp
$Comp
L Device:D D_7_pad1
U 1 1 000000D0
P 16900 2900
F 0 "D_7_pad1" V 16946 2821 50  0000 R CNN
F 1 "D" V 16845 2821 50  0000 R CNN
F 2 "keyboard_parts:D_SOD123_axial" H 16900 2900 50  0001 C CNN
F 3 "~" H 16900 2900 50  0001 C CNN
F 4 "C81598" H 16900 2900 50  0001 C CNN "JLC PCB Part Number"
	1    16900 2900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	16950 2650 16900 2650
Wire Wire Line
	16900 2650 16900 2750
Connection ~ 17550 2650
Connection ~ 16900 3050
Text GLabel 18550 2300 0    50   Input ~ 0
col16
Wire Wire Line
	18550 2300 18550 2650
$Comp
L keyboard_parts:KEYSW K_8_pad1
U 1 1 000000E1
P 18250 2650
F 0 "K_8_pad1" H 18250 2883 60  0000 C CNN
F 1 "KEYSW" H 18250 2550 60  0001 C CNN
F 2 "keyswitches:Kailh_socket_MX_1u" H 18250 2650 60  0001 C CNN
F 3 "" H 18250 2650 60  0000 C CNN
	1    18250 2650
	1    0    0    -1  
$EndComp
$Comp
L Device:D D_8_pad1
U 1 1 000000E0
P 17900 2900
F 0 "D_8_pad1" V 17946 2821 50  0000 R CNN
F 1 "D" V 17845 2821 50  0000 R CNN
F 2 "keyboard_parts:D_SOD123_axial" H 17900 2900 50  0001 C CNN
F 3 "~" H 17900 2900 50  0001 C CNN
F 4 "C81598" H 17900 2900 50  0001 C CNN "JLC PCB Part Number"
	1    17900 2900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	17950 2650 17900 2650
Wire Wire Line
	17900 2650 17900 2750
Connection ~ 18550 2650
Connection ~ 17900 3050
Text GLabel 19550 2300 0    50   Input ~ 0
col17
Wire Wire Line
	19550 2300 19550 2650
$Comp
L keyboard_parts:KEYSW K_9_pad1
U 1 1 000000F1
P 19250 2650
F 0 "K_9_pad1" H 19250 2883 60  0000 C CNN
F 1 "KEYSW" H 19250 2550 60  0001 C CNN
F 2 "keyswitches:Kailh_socket_MX_1u" H 19250 2650 60  0001 C CNN
F 3 "" H 19250 2650 60  0000 C CNN
	1    19250 2650
	1    0    0    -1  
$EndComp
$Comp
L Device:D D_9_pad1
U 1 1 000000F0
P 18900 2900
F 0 "D_9_pad1" V 18946 2821 50  0000 R CNN
F 1 "D" V 18845 2821 50  0000 R CNN
F 2 "keyboard_parts:D_SOD123_axial" H 18900 2900 50  0001 C CNN
F 3 "~" H 18900 2900 50  0001 C CNN
F 4 "C81598" H 18900 2900 50  0001 C CNN "JLC PCB Part Number"
	1    18900 2900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	18950 2650 18900 2650
Wire Wire Line
	18900 2650 18900 2750
Connection ~ 19550 2650
Wire Wire Line
	1900 3050 3900 3050
Text GLabel 1650 4050 0    50   Input ~ 0
row1
$Comp
L keyboard_parts:KEYSW K_4_pad1
U 1 1 00000101
P 17250 3650
F 0 "K_4_pad1" H 17250 3883 60  0000 C CNN
F 1 "KEYSW" H 17250 3550 60  0001 C CNN
F 2 "keyswitches:Kailh_socket_MX_1u" H 17250 3650 60  0001 C CNN
F 3 "" H 17250 3650 60  0000 C CNN
	1    17250 3650
	1    0    0    -1  
$EndComp
$Comp
L Device:D D_4_pad1
U 1 1 00000100
P 16900 3900
F 0 "D_4_pad1" V 16946 3821 50  0000 R CNN
F 1 "D" V 16845 3821 50  0000 R CNN
F 2 "keyboard_parts:D_SOD123_axial" H 16900 3900 50  0001 C CNN
F 3 "~" H 16900 3900 50  0001 C CNN
F 4 "C81598" H 16900 3900 50  0001 C CNN "JLC PCB Part Number"
	1    16900 3900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	16950 3650 16900 3650
Wire Wire Line
	16900 3650 16900 3750
Connection ~ 17550 3650
Connection ~ 16900 4050
$Comp
L keyboard_parts:KEYSW K_5_pad1
U 1 1 00000111
P 18250 3650
F 0 "K_5_pad1" H 18250 3883 60  0000 C CNN
F 1 "KEYSW" H 18250 3550 60  0001 C CNN
F 2 "keyswitches:Kailh_socket_MX_1u" H 18250 3650 60  0001 C CNN
F 3 "" H 18250 3650 60  0000 C CNN
	1    18250 3650
	1    0    0    -1  
$EndComp
$Comp
L Device:D D_5_pad1
U 1 1 00000110
P 17900 3900
F 0 "D_5_pad1" V 17946 3821 50  0000 R CNN
F 1 "D" V 17845 3821 50  0000 R CNN
F 2 "keyboard_parts:D_SOD123_axial" H 17900 3900 50  0001 C CNN
F 3 "~" H 17900 3900 50  0001 C CNN
F 4 "C81598" H 17900 3900 50  0001 C CNN "JLC PCB Part Number"
	1    17900 3900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	17950 3650 17900 3650
Wire Wire Line
	17900 3650 17900 3750
Connection ~ 18550 3650
Connection ~ 17900 4050
$Comp
L keyboard_parts:KEYSW K_6_pad1
U 1 1 00000121
P 19250 3650
F 0 "K_6_pad1" H 19250 3883 60  0000 C CNN
F 1 "KEYSW" H 19250 3550 60  0001 C CNN
F 2 "keyswitches:Kailh_socket_MX_1u" H 19250 3650 60  0001 C CNN
F 3 "" H 19250 3650 60  0000 C CNN
	1    19250 3650
	1    0    0    -1  
$EndComp
$Comp
L Device:D D_6_pad1
U 1 1 00000120
P 18900 3900
F 0 "D_6_pad1" V 18946 3821 50  0000 R CNN
F 1 "D" V 18845 3821 50  0000 R CNN
F 2 "keyboard_parts:D_SOD123_axial" H 18900 3900 50  0001 C CNN
F 3 "~" H 18900 3900 50  0001 C CNN
F 4 "C81598" H 18900 3900 50  0001 C CNN "JLC PCB Part Number"
	1    18900 3900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	18950 3650 18900 3650
Wire Wire Line
	18900 3650 18900 3750
Connection ~ 19550 3650
Text GLabel 1650 5050 0    50   Input ~ 0
row2
Wire Wire Line
	1650 5050 1900 5050
$Comp
L keyboard_parts:KEYSW K_Accent1
U 1 1 00000131
P 2250 4650
F 0 "K_Accent1" H 2250 4883 60  0000 C CNN
F 1 "KEYSW" H 2250 4550 60  0001 C CNN
F 2 "keyswitches:Kailh_socket_MX_1u" H 2250 4650 60  0001 C CNN
F 3 "" H 2250 4650 60  0000 C CNN
	1    2250 4650
	1    0    0    -1  
$EndComp
$Comp
L Device:D D_Accent1
U 1 1 00000130
P 1900 4900
F 0 "D_Accent1" V 1946 4821 50  0000 R CNN
F 1 "D" V 1845 4821 50  0000 R CNN
F 2 "keyboard_parts:D_SOD123_axial" H 1900 4900 50  0001 C CNN
F 3 "~" H 1900 4900 50  0001 C CNN
F 4 "C81598" H 1900 4900 50  0001 C CNN "JLC PCB Part Number"
	1    1900 4900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1950 4650 1900 4650
Wire Wire Line
	1900 4650 1900 4750
Connection ~ 2550 4650
Connection ~ 1900 5050
$Comp
L keyboard_parts:KEYSW K_1
U 1 1 00000141
P 3250 4650
F 0 "K_1" H 3250 4883 60  0000 C CNN
F 1 "KEYSW" H 3250 4550 60  0001 C CNN
F 2 "keyswitches:Kailh_socket_MX_1u" H 3250 4650 60  0001 C CNN
F 3 "" H 3250 4650 60  0000 C CNN
	1    3250 4650
	1    0    0    -1  
$EndComp
$Comp
L Device:D D_1
U 1 1 00000140
P 2900 4900
F 0 "D_1" V 2946 4821 50  0000 R CNN
F 1 "D" V 2845 4821 50  0000 R CNN
F 2 "keyboard_parts:D_SOD123_axial" H 2900 4900 50  0001 C CNN
F 3 "~" H 2900 4900 50  0001 C CNN
F 4 "C81598" H 2900 4900 50  0001 C CNN "JLC PCB Part Number"
	1    2900 4900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2950 4650 2900 4650
Wire Wire Line
	2900 4650 2900 4750
Connection ~ 3550 4650
Connection ~ 2900 5050
$Comp
L keyboard_parts:KEYSW K_2
U 1 1 00000151
P 4250 4650
F 0 "K_2" H 4250 4883 60  0000 C CNN
F 1 "KEYSW" H 4250 4550 60  0001 C CNN
F 2 "keyswitches:Kailh_socket_MX_1u" H 4250 4650 60  0001 C CNN
F 3 "" H 4250 4650 60  0000 C CNN
	1    4250 4650
	1    0    0    -1  
$EndComp
$Comp
L Device:D D_2
U 1 1 00000150
P 3900 4900
F 0 "D_2" V 3946 4821 50  0000 R CNN
F 1 "D" V 3845 4821 50  0000 R CNN
F 2 "keyboard_parts:D_SOD123_axial" H 3900 4900 50  0001 C CNN
F 3 "~" H 3900 4900 50  0001 C CNN
F 4 "C81598" H 3900 4900 50  0001 C CNN "JLC PCB Part Number"
	1    3900 4900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3950 4650 3900 4650
Wire Wire Line
	3900 4650 3900 4750
Connection ~ 4550 4650
Connection ~ 3900 5050
$Comp
L keyboard_parts:KEYSW K_3
U 1 1 00000161
P 5250 4650
F 0 "K_3" H 5250 4883 60  0000 C CNN
F 1 "KEYSW" H 5250 4550 60  0001 C CNN
F 2 "keyswitches:Kailh_socket_MX_1u" H 5250 4650 60  0001 C CNN
F 3 "" H 5250 4650 60  0000 C CNN
	1    5250 4650
	1    0    0    -1  
$EndComp
$Comp
L Device:D D_3
U 1 1 00000160
P 4900 4900
F 0 "D_3" V 4946 4821 50  0000 R CNN
F 1 "D" V 4845 4821 50  0000 R CNN
F 2 "keyboard_parts:D_SOD123_axial" H 4900 4900 50  0001 C CNN
F 3 "~" H 4900 4900 50  0001 C CNN
F 4 "C81598" H 4900 4900 50  0001 C CNN "JLC PCB Part Number"
	1    4900 4900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4950 4650 4900 4650
Wire Wire Line
	4900 4650 4900 4750
Connection ~ 5550 4650
Connection ~ 4900 5050
$Comp
L keyboard_parts:KEYSW K_4
U 1 1 00000171
P 6250 4650
F 0 "K_4" H 6250 4883 60  0000 C CNN
F 1 "KEYSW" H 6250 4550 60  0001 C CNN
F 2 "keyswitches:Kailh_socket_MX_1u" H 6250 4650 60  0001 C CNN
F 3 "" H 6250 4650 60  0000 C CNN
	1    6250 4650
	1    0    0    -1  
$EndComp
$Comp
L Device:D D_4
U 1 1 00000170
P 5900 4900
F 0 "D_4" V 5946 4821 50  0000 R CNN
F 1 "D" V 5845 4821 50  0000 R CNN
F 2 "keyboard_parts:D_SOD123_axial" H 5900 4900 50  0001 C CNN
F 3 "~" H 5900 4900 50  0001 C CNN
F 4 "C81598" H 5900 4900 50  0001 C CNN "JLC PCB Part Number"
	1    5900 4900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5950 4650 5900 4650
Wire Wire Line
	5900 4650 5900 4750
Connection ~ 6550 4650
Connection ~ 5900 5050
$Comp
L keyboard_parts:KEYSW K_5
U 1 1 00000181
P 7250 4650
F 0 "K_5" H 7250 4883 60  0000 C CNN
F 1 "KEYSW" H 7250 4550 60  0001 C CNN
F 2 "keyswitches:Kailh_socket_MX_1u" H 7250 4650 60  0001 C CNN
F 3 "" H 7250 4650 60  0000 C CNN
	1    7250 4650
	1    0    0    -1  
$EndComp
$Comp
L Device:D D_5
U 1 1 00000180
P 6900 4900
F 0 "D_5" V 6946 4821 50  0000 R CNN
F 1 "D" V 6845 4821 50  0000 R CNN
F 2 "keyboard_parts:D_SOD123_axial" H 6900 4900 50  0001 C CNN
F 3 "~" H 6900 4900 50  0001 C CNN
F 4 "C81598" H 6900 4900 50  0001 C CNN "JLC PCB Part Number"
	1    6900 4900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6950 4650 6900 4650
Wire Wire Line
	6900 4650 6900 4750
Connection ~ 7550 4650
Connection ~ 6900 5050
$Comp
L keyboard_parts:KEYSW K_6
U 1 1 00000191
P 8250 4650
F 0 "K_6" H 8250 4883 60  0000 C CNN
F 1 "KEYSW" H 8250 4550 60  0001 C CNN
F 2 "keyswitches:Kailh_socket_MX_1u" H 8250 4650 60  0001 C CNN
F 3 "" H 8250 4650 60  0000 C CNN
	1    8250 4650
	1    0    0    -1  
$EndComp
$Comp
L Device:D D_6
U 1 1 00000190
P 7900 4900
F 0 "D_6" V 7946 4821 50  0000 R CNN
F 1 "D" V 7845 4821 50  0000 R CNN
F 2 "keyboard_parts:D_SOD123_axial" H 7900 4900 50  0001 C CNN
F 3 "~" H 7900 4900 50  0001 C CNN
F 4 "C81598" H 7900 4900 50  0001 C CNN "JLC PCB Part Number"
	1    7900 4900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7950 4650 7900 4650
Wire Wire Line
	7900 4650 7900 4750
Connection ~ 8550 4650
Connection ~ 7900 5050
$Comp
L keyboard_parts:KEYSW K_7
U 1 1 000001A1
P 9250 4650
F 0 "K_7" H 9250 4883 60  0000 C CNN
F 1 "KEYSW" H 9250 4550 60  0001 C CNN
F 2 "keyswitches:Kailh_socket_MX_1u" H 9250 4650 60  0001 C CNN
F 3 "" H 9250 4650 60  0000 C CNN
	1    9250 4650
	1    0    0    -1  
$EndComp
$Comp
L Device:D D_7
U 1 1 000001A0
P 8900 4900
F 0 "D_7" V 8946 4821 50  0000 R CNN
F 1 "D" V 8845 4821 50  0000 R CNN
F 2 "keyboard_parts:D_SOD123_axial" H 8900 4900 50  0001 C CNN
F 3 "~" H 8900 4900 50  0001 C CNN
F 4 "C81598" H 8900 4900 50  0001 C CNN "JLC PCB Part Number"
	1    8900 4900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8950 4650 8900 4650
Wire Wire Line
	8900 4650 8900 4750
Connection ~ 9550 4650
Connection ~ 8900 5050
$Comp
L keyboard_parts:KEYSW K_8
U 1 1 000001B1
P 10250 4650
F 0 "K_8" H 10250 4883 60  0000 C CNN
F 1 "KEYSW" H 10250 4550 60  0001 C CNN
F 2 "keyswitches:Kailh_socket_MX_1u" H 10250 4650 60  0001 C CNN
F 3 "" H 10250 4650 60  0000 C CNN
	1    10250 4650
	1    0    0    -1  
$EndComp
$Comp
L Device:D D_8
U 1 1 000001B0
P 9900 4900
F 0 "D_8" V 9946 4821 50  0000 R CNN
F 1 "D" V 9845 4821 50  0000 R CNN
F 2 "keyboard_parts:D_SOD123_axial" H 9900 4900 50  0001 C CNN
F 3 "~" H 9900 4900 50  0001 C CNN
F 4 "C81598" H 9900 4900 50  0001 C CNN "JLC PCB Part Number"
	1    9900 4900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	9950 4650 9900 4650
Wire Wire Line
	9900 4650 9900 4750
Connection ~ 10550 4650
Connection ~ 9900 5050
$Comp
L keyboard_parts:KEYSW K_9
U 1 1 000001C1
P 11250 4650
F 0 "K_9" H 11250 4883 60  0000 C CNN
F 1 "KEYSW" H 11250 4550 60  0001 C CNN
F 2 "keyswitches:Kailh_socket_MX_1u" H 11250 4650 60  0001 C CNN
F 3 "" H 11250 4650 60  0000 C CNN
	1    11250 4650
	1    0    0    -1  
$EndComp
$Comp
L Device:D D_9
U 1 1 000001C0
P 10900 4900
F 0 "D_9" V 10946 4821 50  0000 R CNN
F 1 "D" V 10845 4821 50  0000 R CNN
F 2 "keyboard_parts:D_SOD123_axial" H 10900 4900 50  0001 C CNN
F 3 "~" H 10900 4900 50  0001 C CNN
F 4 "C81598" H 10900 4900 50  0001 C CNN "JLC PCB Part Number"
	1    10900 4900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	10950 4650 10900 4650
Wire Wire Line
	10900 4650 10900 4750
Connection ~ 11550 4650
Connection ~ 10900 5050
$Comp
L keyboard_parts:KEYSW K_0
U 1 1 000001D1
P 12250 4650
F 0 "K_0" H 12250 4883 60  0000 C CNN
F 1 "KEYSW" H 12250 4550 60  0001 C CNN
F 2 "keyswitches:Kailh_socket_MX_1u" H 12250 4650 60  0001 C CNN
F 3 "" H 12250 4650 60  0000 C CNN
	1    12250 4650
	1    0    0    -1  
$EndComp
$Comp
L Device:D D_0
U 1 1 000001D0
P 11900 4900
F 0 "D_0" V 11946 4821 50  0000 R CNN
F 1 "D" V 11845 4821 50  0000 R CNN
F 2 "keyboard_parts:D_SOD123_axial" H 11900 4900 50  0001 C CNN
F 3 "~" H 11900 4900 50  0001 C CNN
F 4 "C81598" H 11900 4900 50  0001 C CNN "JLC PCB Part Number"
	1    11900 4900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	11950 4650 11900 4650
Wire Wire Line
	11900 4650 11900 4750
Connection ~ 12550 4650
Connection ~ 11900 5050
$Comp
L keyboard_parts:KEYSW K_Minus1
U 1 1 000001E1
P 13250 4650
F 0 "K_Minus1" H 13250 4883 60  0000 C CNN
F 1 "KEYSW" H 13250 4550 60  0001 C CNN
F 2 "keyswitches:Kailh_socket_MX_1u" H 13250 4650 60  0001 C CNN
F 3 "" H 13250 4650 60  0000 C CNN
	1    13250 4650
	1    0    0    -1  
$EndComp
$Comp
L Device:D D_Minus1
U 1 1 000001E0
P 12900 4900
F 0 "D_Minus1" V 12946 4821 50  0000 R CNN
F 1 "D" V 12845 4821 50  0000 R CNN
F 2 "keyboard_parts:D_SOD123_axial" H 12900 4900 50  0001 C CNN
F 3 "~" H 12900 4900 50  0001 C CNN
F 4 "C81598" H 12900 4900 50  0001 C CNN "JLC PCB Part Number"
	1    12900 4900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	12950 4650 12900 4650
Wire Wire Line
	12900 4650 12900 4750
Connection ~ 13550 4650
Connection ~ 12900 5050
$Comp
L keyboard_parts:KEYSW K_Del1
U 1 1 000001F1
P 14250 4650
F 0 "K_Del1" H 14250 4883 60  0000 C CNN
F 1 "KEYSW" H 14250 4550 60  0001 C CNN
F 2 "keyswitches:Kailh_socket_MX_1u" H 14250 4650 60  0001 C CNN
F 3 "" H 14250 4650 60  0000 C CNN
	1    14250 4650
	1    0    0    -1  
$EndComp
$Comp
L Device:D D_Del1
U 1 1 000001F0
P 13900 4900
F 0 "D_Del1" V 13946 4821 50  0000 R CNN
F 1 "D" V 13845 4821 50  0000 R CNN
F 2 "keyboard_parts:D_SOD123_axial" H 13900 4900 50  0001 C CNN
F 3 "~" H 13900 4900 50  0001 C CNN
F 4 "C81598" H 13900 4900 50  0001 C CNN "JLC PCB Part Number"
	1    13900 4900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	13950 4650 13900 4650
Wire Wire Line
	13900 4650 13900 4750
Connection ~ 14550 4650
Connection ~ 13900 5050
$Comp
L keyboard_parts:KEYSW K_Backspace1
U 1 1 00000201
P 16250 4650
F 0 "K_Backspace1" H 16250 4883 60  0000 C CNN
F 1 "KEYSW" H 16250 4550 60  0001 C CNN
F 2 "keyswitches:Kailh_socket_MX_2u" H 16250 4650 60  0001 C CNN
F 3 "" H 16250 4650 60  0000 C CNN
	1    16250 4650
	1    0    0    -1  
$EndComp
$Comp
L Device:D D_Backspace1
U 1 1 00000200
P 15900 4900
F 0 "D_Backspace1" V 15946 4821 50  0000 R CNN
F 1 "D" V 15845 4821 50  0000 R CNN
F 2 "keyboard_parts:D_SOD123_axial" H 15900 4900 50  0001 C CNN
F 3 "~" H 15900 4900 50  0001 C CNN
F 4 "C81598" H 15900 4900 50  0001 C CNN "JLC PCB Part Number"
	1    15900 4900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	15950 4650 15900 4650
Wire Wire Line
	15900 4650 15900 4750
Connection ~ 16550 4650
Connection ~ 15900 5050
$Comp
L keyboard_parts:KEYSW K_1_pad1
U 1 1 00000211
P 17250 4650
F 0 "K_1_pad1" H 17250 4883 60  0000 C CNN
F 1 "KEYSW" H 17250 4550 60  0001 C CNN
F 2 "keyswitches:Kailh_socket_MX_1u" H 17250 4650 60  0001 C CNN
F 3 "" H 17250 4650 60  0000 C CNN
	1    17250 4650
	1    0    0    -1  
$EndComp
$Comp
L Device:D D_1_pad1
U 1 1 00000210
P 16900 4900
F 0 "D_1_pad1" V 16946 4821 50  0000 R CNN
F 1 "D" V 16845 4821 50  0000 R CNN
F 2 "keyboard_parts:D_SOD123_axial" H 16900 4900 50  0001 C CNN
F 3 "~" H 16900 4900 50  0001 C CNN
F 4 "C81598" H 16900 4900 50  0001 C CNN "JLC PCB Part Number"
	1    16900 4900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	16950 4650 16900 4650
Wire Wire Line
	16900 4650 16900 4750
Connection ~ 17550 4650
Connection ~ 16900 5050
$Comp
L keyboard_parts:KEYSW K_2_pad1
U 1 1 00000221
P 18250 4650
F 0 "K_2_pad1" H 18250 4883 60  0000 C CNN
F 1 "KEYSW" H 18250 4550 60  0001 C CNN
F 2 "keyswitches:Kailh_socket_MX_1u" H 18250 4650 60  0001 C CNN
F 3 "" H 18250 4650 60  0000 C CNN
	1    18250 4650
	1    0    0    -1  
$EndComp
$Comp
L Device:D D_2_pad1
U 1 1 00000220
P 17900 4900
F 0 "D_2_pad1" V 17946 4821 50  0000 R CNN
F 1 "D" V 17845 4821 50  0000 R CNN
F 2 "keyboard_parts:D_SOD123_axial" H 17900 4900 50  0001 C CNN
F 3 "~" H 17900 4900 50  0001 C CNN
F 4 "C81598" H 17900 4900 50  0001 C CNN "JLC PCB Part Number"
	1    17900 4900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	17950 4650 17900 4650
Wire Wire Line
	17900 4650 17900 4750
Connection ~ 18550 4650
Connection ~ 17900 5050
$Comp
L keyboard_parts:KEYSW K_3_pad1
U 1 1 00000231
P 19250 4650
F 0 "K_3_pad1" H 19250 4883 60  0000 C CNN
F 1 "KEYSW" H 19250 4550 60  0001 C CNN
F 2 "keyswitches:Kailh_socket_MX_1u" H 19250 4650 60  0001 C CNN
F 3 "" H 19250 4650 60  0000 C CNN
	1    19250 4650
	1    0    0    -1  
$EndComp
$Comp
L Device:D D_3_pad1
U 1 1 00000230
P 18900 4900
F 0 "D_3_pad1" V 18946 4821 50  0000 R CNN
F 1 "D" V 18845 4821 50  0000 R CNN
F 2 "keyboard_parts:D_SOD123_axial" H 18900 4900 50  0001 C CNN
F 3 "~" H 18900 4900 50  0001 C CNN
F 4 "C81598" H 18900 4900 50  0001 C CNN "JLC PCB Part Number"
	1    18900 4900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	18950 4650 18900 4650
Wire Wire Line
	18900 4650 18900 4750
Connection ~ 19550 4650
Wire Wire Line
	1900 5050 2900 5050
Text GLabel 1650 6050 0    50   Input ~ 0
row3
Wire Wire Line
	1650 6050 1900 6050
$Comp
L keyboard_parts:KEYSW K_Tab1
U 1 1 00000241
P 2250 5650
F 0 "K_Tab1" H 2250 5883 60  0000 C CNN
F 1 "KEYSW" H 2250 5550 60  0001 C CNN
F 2 "keyswitches:Kailh_socket_MX_1.5u" H 2250 5650 60  0001 C CNN
F 3 "" H 2250 5650 60  0000 C CNN
	1    2250 5650
	1    0    0    -1  
$EndComp
$Comp
L Device:D D_Tab1
U 1 1 00000240
P 1900 5900
F 0 "D_Tab1" V 1946 5821 50  0000 R CNN
F 1 "D" V 1845 5821 50  0000 R CNN
F 2 "keyboard_parts:D_SOD123_axial" H 1900 5900 50  0001 C CNN
F 3 "~" H 1900 5900 50  0001 C CNN
F 4 "C81598" H 1900 5900 50  0001 C CNN "JLC PCB Part Number"
	1    1900 5900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1950 5650 1900 5650
Wire Wire Line
	1900 5650 1900 5750
Connection ~ 2550 5650
Connection ~ 1900 6050
$Comp
L keyboard_parts:KEYSW K_Q1
U 1 1 00000251
P 4250 5650
F 0 "K_Q1" H 4250 5883 60  0000 C CNN
F 1 "KEYSW" H 4250 5550 60  0001 C CNN
F 2 "keyswitches:Kailh_socket_MX_1u" H 4250 5650 60  0001 C CNN
F 3 "" H 4250 5650 60  0000 C CNN
	1    4250 5650
	1    0    0    -1  
$EndComp
$Comp
L Device:D D_Q1
U 1 1 00000250
P 3900 5900
F 0 "D_Q1" V 3946 5821 50  0000 R CNN
F 1 "D" V 3845 5821 50  0000 R CNN
F 2 "keyboard_parts:D_SOD123_axial" H 3900 5900 50  0001 C CNN
F 3 "~" H 3900 5900 50  0001 C CNN
F 4 "C81598" H 3900 5900 50  0001 C CNN "JLC PCB Part Number"
	1    3900 5900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3950 5650 3900 5650
Wire Wire Line
	3900 5650 3900 5750
Connection ~ 4550 5650
Connection ~ 3900 6050
$Comp
L keyboard_parts:KEYSW K_W1
U 1 1 00000261
P 5250 5650
F 0 "K_W1" H 5250 5883 60  0000 C CNN
F 1 "KEYSW" H 5250 5550 60  0001 C CNN
F 2 "keyswitches:Kailh_socket_MX_1u" H 5250 5650 60  0001 C CNN
F 3 "" H 5250 5650 60  0000 C CNN
	1    5250 5650
	1    0    0    -1  
$EndComp
$Comp
L Device:D D_W1
U 1 1 00000260
P 4900 5900
F 0 "D_W1" V 4946 5821 50  0000 R CNN
F 1 "D" V 4845 5821 50  0000 R CNN
F 2 "keyboard_parts:D_SOD123_axial" H 4900 5900 50  0001 C CNN
F 3 "~" H 4900 5900 50  0001 C CNN
F 4 "C81598" H 4900 5900 50  0001 C CNN "JLC PCB Part Number"
	1    4900 5900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4950 5650 4900 5650
Wire Wire Line
	4900 5650 4900 5750
Connection ~ 5550 5650
Connection ~ 4900 6050
$Comp
L keyboard_parts:KEYSW K_E1
U 1 1 00000271
P 6250 5650
F 0 "K_E1" H 6250 5883 60  0000 C CNN
F 1 "KEYSW" H 6250 5550 60  0001 C CNN
F 2 "keyswitches:Kailh_socket_MX_1u" H 6250 5650 60  0001 C CNN
F 3 "" H 6250 5650 60  0000 C CNN
	1    6250 5650
	1    0    0    -1  
$EndComp
$Comp
L Device:D D_E1
U 1 1 00000270
P 5900 5900
F 0 "D_E1" V 5946 5821 50  0000 R CNN
F 1 "D" V 5845 5821 50  0000 R CNN
F 2 "keyboard_parts:D_SOD123_axial" H 5900 5900 50  0001 C CNN
F 3 "~" H 5900 5900 50  0001 C CNN
F 4 "C81598" H 5900 5900 50  0001 C CNN "JLC PCB Part Number"
	1    5900 5900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5950 5650 5900 5650
Wire Wire Line
	5900 5650 5900 5750
Connection ~ 6550 5650
Connection ~ 5900 6050
$Comp
L keyboard_parts:KEYSW K_R1
U 1 1 00000281
P 7250 5650
F 0 "K_R1" H 7250 5883 60  0000 C CNN
F 1 "KEYSW" H 7250 5550 60  0001 C CNN
F 2 "keyswitches:Kailh_socket_MX_1u" H 7250 5650 60  0001 C CNN
F 3 "" H 7250 5650 60  0000 C CNN
	1    7250 5650
	1    0    0    -1  
$EndComp
$Comp
L Device:D D_R1
U 1 1 00000280
P 6900 5900
F 0 "D_R1" V 6946 5821 50  0000 R CNN
F 1 "D" V 6845 5821 50  0000 R CNN
F 2 "keyboard_parts:D_SOD123_axial" H 6900 5900 50  0001 C CNN
F 3 "~" H 6900 5900 50  0001 C CNN
F 4 "C81598" H 6900 5900 50  0001 C CNN "JLC PCB Part Number"
	1    6900 5900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6950 5650 6900 5650
Wire Wire Line
	6900 5650 6900 5750
Connection ~ 7550 5650
Connection ~ 6900 6050
$Comp
L keyboard_parts:KEYSW K_T1
U 1 1 00000291
P 8250 5650
F 0 "K_T1" H 8250 5883 60  0000 C CNN
F 1 "KEYSW" H 8250 5550 60  0001 C CNN
F 2 "keyswitches:Kailh_socket_MX_1u" H 8250 5650 60  0001 C CNN
F 3 "" H 8250 5650 60  0000 C CNN
	1    8250 5650
	1    0    0    -1  
$EndComp
$Comp
L Device:D D_T1
U 1 1 00000290
P 7900 5900
F 0 "D_T1" V 7946 5821 50  0000 R CNN
F 1 "D" V 7845 5821 50  0000 R CNN
F 2 "keyboard_parts:D_SOD123_axial" H 7900 5900 50  0001 C CNN
F 3 "~" H 7900 5900 50  0001 C CNN
F 4 "C81598" H 7900 5900 50  0001 C CNN "JLC PCB Part Number"
	1    7900 5900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7950 5650 7900 5650
Wire Wire Line
	7900 5650 7900 5750
Connection ~ 8550 5650
Connection ~ 7900 6050
$Comp
L keyboard_parts:KEYSW K_Y1
U 1 1 000002A1
P 9250 5650
F 0 "K_Y1" H 9250 5883 60  0000 C CNN
F 1 "KEYSW" H 9250 5550 60  0001 C CNN
F 2 "keyswitches:Kailh_socket_MX_1u" H 9250 5650 60  0001 C CNN
F 3 "" H 9250 5650 60  0000 C CNN
	1    9250 5650
	1    0    0    -1  
$EndComp
$Comp
L Device:D D_Y1
U 1 1 000002A0
P 8900 5900
F 0 "D_Y1" V 8946 5821 50  0000 R CNN
F 1 "D" V 8845 5821 50  0000 R CNN
F 2 "keyboard_parts:D_SOD123_axial" H 8900 5900 50  0001 C CNN
F 3 "~" H 8900 5900 50  0001 C CNN
F 4 "C81598" H 8900 5900 50  0001 C CNN "JLC PCB Part Number"
	1    8900 5900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8950 5650 8900 5650
Wire Wire Line
	8900 5650 8900 5750
Connection ~ 9550 5650
Connection ~ 8900 6050
$Comp
L keyboard_parts:KEYSW K_U1
U 1 1 000002B1
P 10250 5650
F 0 "K_U1" H 10250 5883 60  0000 C CNN
F 1 "KEYSW" H 10250 5550 60  0001 C CNN
F 2 "keyswitches:Kailh_socket_MX_1u" H 10250 5650 60  0001 C CNN
F 3 "" H 10250 5650 60  0000 C CNN
	1    10250 5650
	1    0    0    -1  
$EndComp
$Comp
L Device:D D_U1
U 1 1 000002B0
P 9900 5900
F 0 "D_U1" V 9946 5821 50  0000 R CNN
F 1 "D" V 9845 5821 50  0000 R CNN
F 2 "keyboard_parts:D_SOD123_axial" H 9900 5900 50  0001 C CNN
F 3 "~" H 9900 5900 50  0001 C CNN
F 4 "C81598" H 9900 5900 50  0001 C CNN "JLC PCB Part Number"
	1    9900 5900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	9950 5650 9900 5650
Wire Wire Line
	9900 5650 9900 5750
Connection ~ 10550 5650
Connection ~ 9900 6050
$Comp
L keyboard_parts:KEYSW K_I1
U 1 1 000002C1
P 11250 5650
F 0 "K_I1" H 11250 5883 60  0000 C CNN
F 1 "KEYSW" H 11250 5550 60  0001 C CNN
F 2 "keyswitches:Kailh_socket_MX_1u" H 11250 5650 60  0001 C CNN
F 3 "" H 11250 5650 60  0000 C CNN
	1    11250 5650
	1    0    0    -1  
$EndComp
$Comp
L Device:D D_I1
U 1 1 000002C0
P 10900 5900
F 0 "D_I1" V 10946 5821 50  0000 R CNN
F 1 "D" V 10845 5821 50  0000 R CNN
F 2 "keyboard_parts:D_SOD123_axial" H 10900 5900 50  0001 C CNN
F 3 "~" H 10900 5900 50  0001 C CNN
F 4 "C81598" H 10900 5900 50  0001 C CNN "JLC PCB Part Number"
	1    10900 5900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	10950 5650 10900 5650
Wire Wire Line
	10900 5650 10900 5750
Connection ~ 11550 5650
Connection ~ 10900 6050
$Comp
L keyboard_parts:KEYSW K_O1
U 1 1 000002D1
P 12250 5650
F 0 "K_O1" H 12250 5883 60  0000 C CNN
F 1 "KEYSW" H 12250 5550 60  0001 C CNN
F 2 "keyswitches:Kailh_socket_MX_1u" H 12250 5650 60  0001 C CNN
F 3 "" H 12250 5650 60  0000 C CNN
	1    12250 5650
	1    0    0    -1  
$EndComp
$Comp
L Device:D D_O1
U 1 1 000002D0
P 11900 5900
F 0 "D_O1" V 11946 5821 50  0000 R CNN
F 1 "D" V 11845 5821 50  0000 R CNN
F 2 "keyboard_parts:D_SOD123_axial" H 11900 5900 50  0001 C CNN
F 3 "~" H 11900 5900 50  0001 C CNN
F 4 "C81598" H 11900 5900 50  0001 C CNN "JLC PCB Part Number"
	1    11900 5900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	11950 5650 11900 5650
Wire Wire Line
	11900 5650 11900 5750
Connection ~ 12550 5650
Connection ~ 11900 6050
$Comp
L keyboard_parts:KEYSW K_P1
U 1 1 000002E1
P 13250 5650
F 0 "K_P1" H 13250 5883 60  0000 C CNN
F 1 "KEYSW" H 13250 5550 60  0001 C CNN
F 2 "keyswitches:Kailh_socket_MX_1u" H 13250 5650 60  0001 C CNN
F 3 "" H 13250 5650 60  0000 C CNN
	1    13250 5650
	1    0    0    -1  
$EndComp
$Comp
L Device:D D_P1
U 1 1 000002E0
P 12900 5900
F 0 "D_P1" V 12946 5821 50  0000 R CNN
F 1 "D" V 12845 5821 50  0000 R CNN
F 2 "keyboard_parts:D_SOD123_axial" H 12900 5900 50  0001 C CNN
F 3 "~" H 12900 5900 50  0001 C CNN
F 4 "C81598" H 12900 5900 50  0001 C CNN "JLC PCB Part Number"
	1    12900 5900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	12950 5650 12900 5650
Wire Wire Line
	12900 5650 12900 5750
Connection ~ 13550 5650
Connection ~ 12900 6050
$Comp
L keyboard_parts:KEYSW K_LftBrack1
U 1 1 000002F1
P 14250 5650
F 0 "K_LftBrack1" H 14250 5883 60  0000 C CNN
F 1 "KEYSW" H 14250 5550 60  0001 C CNN
F 2 "keyswitches:Kailh_socket_MX_1u" H 14250 5650 60  0001 C CNN
F 3 "" H 14250 5650 60  0000 C CNN
	1    14250 5650
	1    0    0    -1  
$EndComp
$Comp
L Device:D D_LftBrack1
U 1 1 000002F0
P 13900 5900
F 0 "D_LftBrack1" V 13946 5821 50  0000 R CNN
F 1 "D" V 13845 5821 50  0000 R CNN
F 2 "keyboard_parts:D_SOD123_axial" H 13900 5900 50  0001 C CNN
F 3 "~" H 13900 5900 50  0001 C CNN
F 4 "C81598" H 13900 5900 50  0001 C CNN "JLC PCB Part Number"
	1    13900 5900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	13950 5650 13900 5650
Wire Wire Line
	13900 5650 13900 5750
Connection ~ 14550 5650
Connection ~ 13900 6050
$Comp
L keyboard_parts:KEYSW K_RgtBrack1
U 1 1 00000301
P 15250 5650
F 0 "K_RgtBrack1" H 15250 5883 60  0000 C CNN
F 1 "KEYSW" H 15250 5550 60  0001 C CNN
F 2 "keyswitches:Kailh_socket_MX_1u" H 15250 5650 60  0001 C CNN
F 3 "" H 15250 5650 60  0000 C CNN
	1    15250 5650
	1    0    0    -1  
$EndComp
$Comp
L Device:D D_RgtBrack1
U 1 1 00000300
P 14900 5900
F 0 "D_RgtBrack1" V 14946 5821 50  0000 R CNN
F 1 "D" V 14845 5821 50  0000 R CNN
F 2 "keyboard_parts:D_SOD123_axial" H 14900 5900 50  0001 C CNN
F 3 "~" H 14900 5900 50  0001 C CNN
F 4 "C81598" H 14900 5900 50  0001 C CNN "JLC PCB Part Number"
	1    14900 5900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	14950 5650 14900 5650
Wire Wire Line
	14900 5650 14900 5750
Connection ~ 15550 5650
Connection ~ 14900 6050
$Comp
L keyboard_parts:KEYSW K_BckSlash1
U 1 1 00000311
P 16250 5650
F 0 "K_BckSlash1" H 16250 5883 60  0000 C CNN
F 1 "KEYSW" H 16250 5550 60  0001 C CNN
F 2 "keyswitches:Kailh_socket_MX_1.5u" H 16250 5650 60  0001 C CNN
F 3 "" H 16250 5650 60  0000 C CNN
	1    16250 5650
	1    0    0    -1  
$EndComp
$Comp
L Device:D D_BckSlash1
U 1 1 00000310
P 15900 5900
F 0 "D_BckSlash1" V 15946 5821 50  0000 R CNN
F 1 "D" V 15845 5821 50  0000 R CNN
F 2 "keyboard_parts:D_SOD123_axial" H 15900 5900 50  0001 C CNN
F 3 "~" H 15900 5900 50  0001 C CNN
F 4 "C81598" H 15900 5900 50  0001 C CNN "JLC PCB Part Number"
	1    15900 5900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	15950 5650 15900 5650
Wire Wire Line
	15900 5650 15900 5750
Connection ~ 16550 5650
Connection ~ 15900 6050
$Comp
L keyboard_parts:KEYSW K_Equals_pad1
U 1 1 00000321
P 17250 5650
F 0 "K_Equals_pad1" H 17250 5883 60  0000 C CNN
F 1 "KEYSW" H 17250 5550 60  0001 C CNN
F 2 "keyswitches:Kailh_socket_MX_1u" H 17250 5650 60  0001 C CNN
F 3 "" H 17250 5650 60  0000 C CNN
	1    17250 5650
	1    0    0    -1  
$EndComp
$Comp
L Device:D D_Equals_pad1
U 1 1 00000320
P 16900 5900
F 0 "D_Equals_pad1" V 16946 5821 50  0000 R CNN
F 1 "D" V 16845 5821 50  0000 R CNN
F 2 "keyboard_parts:D_SOD123_axial" H 16900 5900 50  0001 C CNN
F 3 "~" H 16900 5900 50  0001 C CNN
F 4 "C81598" H 16900 5900 50  0001 C CNN "JLC PCB Part Number"
	1    16900 5900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	16950 5650 16900 5650
Wire Wire Line
	16900 5650 16900 5750
Connection ~ 17550 5650
Connection ~ 16900 6050
$Comp
L keyboard_parts:KEYSW K_0_pad1
U 1 1 00000331
P 18250 5650
F 0 "K_0_pad1" H 18250 5883 60  0000 C CNN
F 1 "KEYSW" H 18250 5550 60  0001 C CNN
F 2 "keyswitches:Kailh_socket_MX_1u" H 18250 5650 60  0001 C CNN
F 3 "" H 18250 5650 60  0000 C CNN
	1    18250 5650
	1    0    0    -1  
$EndComp
$Comp
L Device:D D_0_pad1
U 1 1 00000330
P 17900 5900
F 0 "D_0_pad1" V 17946 5821 50  0000 R CNN
F 1 "D" V 17845 5821 50  0000 R CNN
F 2 "keyboard_parts:D_SOD123_axial" H 17900 5900 50  0001 C CNN
F 3 "~" H 17900 5900 50  0001 C CNN
F 4 "C81598" H 17900 5900 50  0001 C CNN "JLC PCB Part Number"
	1    17900 5900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	17950 5650 17900 5650
Wire Wire Line
	17900 5650 17900 5750
Connection ~ 18550 5650
Connection ~ 17900 6050
$Comp
L keyboard_parts:KEYSW K_Point_pad1
U 1 1 00000341
P 19250 5650
F 0 "K_Point_pad1" H 19250 5883 60  0000 C CNN
F 1 "KEYSW" H 19250 5550 60  0001 C CNN
F 2 "keyswitches:Kailh_socket_MX_1u" H 19250 5650 60  0001 C CNN
F 3 "" H 19250 5650 60  0000 C CNN
	1    19250 5650
	1    0    0    -1  
$EndComp
$Comp
L Device:D D_Point_pad1
U 1 1 00000340
P 18900 5900
F 0 "D_Point_pad1" V 18946 5821 50  0000 R CNN
F 1 "D" V 18845 5821 50  0000 R CNN
F 2 "keyboard_parts:D_SOD123_axial" H 18900 5900 50  0001 C CNN
F 3 "~" H 18900 5900 50  0001 C CNN
F 4 "C81598" H 18900 5900 50  0001 C CNN "JLC PCB Part Number"
	1    18900 5900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	18950 5650 18900 5650
Wire Wire Line
	18900 5650 18900 5750
Connection ~ 19550 5650
Wire Wire Line
	1900 6050 3900 6050
Text GLabel 1650 7050 0    50   Input ~ 0
row4
Wire Wire Line
	1650 7050 1900 7050
$Comp
L keyboard_parts:KEYSW K_Caps1
U 1 1 00000351
P 2250 6650
F 0 "K_Caps1" H 2250 6883 60  0000 C CNN
F 1 "KEYSW" H 2250 6550 60  0001 C CNN
F 2 "keyswitches:Kailh_socket_MX_1.75u" H 2250 6650 60  0001 C CNN
F 3 "" H 2250 6650 60  0000 C CNN
	1    2250 6650
	1    0    0    -1  
$EndComp
$Comp
L Device:D D_Caps1
U 1 1 00000350
P 1900 6900
F 0 "D_Caps1" V 1946 6821 50  0000 R CNN
F 1 "D" V 1845 6821 50  0000 R CNN
F 2 "keyboard_parts:D_SOD123_axial" H 1900 6900 50  0001 C CNN
F 3 "~" H 1900 6900 50  0001 C CNN
F 4 "C81598" H 1900 6900 50  0001 C CNN "JLC PCB Part Number"
	1    1900 6900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1950 6650 1900 6650
Wire Wire Line
	1900 6650 1900 6750
Connection ~ 2550 6650
Connection ~ 1900 7050
$Comp
L keyboard_parts:KEYSW K_A1
U 1 1 00000361
P 4250 6650
F 0 "K_A1" H 4250 6883 60  0000 C CNN
F 1 "KEYSW" H 4250 6550 60  0001 C CNN
F 2 "keyswitches:Kailh_socket_MX_1u" H 4250 6650 60  0001 C CNN
F 3 "" H 4250 6650 60  0000 C CNN
	1    4250 6650
	1    0    0    -1  
$EndComp
$Comp
L Device:D D_A1
U 1 1 00000360
P 3900 6900
F 0 "D_A1" V 3946 6821 50  0000 R CNN
F 1 "D" V 3845 6821 50  0000 R CNN
F 2 "keyboard_parts:D_SOD123_axial" H 3900 6900 50  0001 C CNN
F 3 "~" H 3900 6900 50  0001 C CNN
F 4 "C81598" H 3900 6900 50  0001 C CNN "JLC PCB Part Number"
	1    3900 6900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3950 6650 3900 6650
Wire Wire Line
	3900 6650 3900 6750
Connection ~ 4550 6650
Connection ~ 3900 7050
$Comp
L keyboard_parts:KEYSW K_S1
U 1 1 00000371
P 5250 6650
F 0 "K_S1" H 5250 6883 60  0000 C CNN
F 1 "KEYSW" H 5250 6550 60  0001 C CNN
F 2 "keyswitches:Kailh_socket_MX_1u" H 5250 6650 60  0001 C CNN
F 3 "" H 5250 6650 60  0000 C CNN
	1    5250 6650
	1    0    0    -1  
$EndComp
$Comp
L Device:D D_S1
U 1 1 00000370
P 4900 6900
F 0 "D_S1" V 4946 6821 50  0000 R CNN
F 1 "D" V 4845 6821 50  0000 R CNN
F 2 "keyboard_parts:D_SOD123_axial" H 4900 6900 50  0001 C CNN
F 3 "~" H 4900 6900 50  0001 C CNN
F 4 "C81598" H 4900 6900 50  0001 C CNN "JLC PCB Part Number"
	1    4900 6900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4950 6650 4900 6650
Wire Wire Line
	4900 6650 4900 6750
Connection ~ 5550 6650
Connection ~ 4900 7050
$Comp
L keyboard_parts:KEYSW K_D1
U 1 1 00000381
P 6250 6650
F 0 "K_D1" H 6250 6883 60  0000 C CNN
F 1 "KEYSW" H 6250 6550 60  0001 C CNN
F 2 "keyswitches:Kailh_socket_MX_1u" H 6250 6650 60  0001 C CNN
F 3 "" H 6250 6650 60  0000 C CNN
	1    6250 6650
	1    0    0    -1  
$EndComp
$Comp
L Device:D D_D1
U 1 1 00000380
P 5900 6900
F 0 "D_D1" V 5946 6821 50  0000 R CNN
F 1 "D" V 5845 6821 50  0000 R CNN
F 2 "keyboard_parts:D_SOD123_axial" H 5900 6900 50  0001 C CNN
F 3 "~" H 5900 6900 50  0001 C CNN
F 4 "C81598" H 5900 6900 50  0001 C CNN "JLC PCB Part Number"
	1    5900 6900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5950 6650 5900 6650
Wire Wire Line
	5900 6650 5900 6750
Connection ~ 6550 6650
Connection ~ 5900 7050
Wire Wire Line
	6950 6650 6900 6650
Wire Wire Line
	6900 6650 6900 6750
Connection ~ 7550 6650
Connection ~ 6900 7050
$Comp
L keyboard_parts:KEYSW K_G1
U 1 1 000003A1
P 8250 6650
F 0 "K_G1" H 8250 6883 60  0000 C CNN
F 1 "KEYSW" H 8250 6550 60  0001 C CNN
F 2 "keyswitches:Kailh_socket_MX_1u" H 8250 6650 60  0001 C CNN
F 3 "" H 8250 6650 60  0000 C CNN
	1    8250 6650
	1    0    0    -1  
$EndComp
$Comp
L Device:D D_G1
U 1 1 000003A0
P 7900 6900
F 0 "D_G1" V 7946 6821 50  0000 R CNN
F 1 "D" V 7845 6821 50  0000 R CNN
F 2 "keyboard_parts:D_SOD123_axial" H 7900 6900 50  0001 C CNN
F 3 "~" H 7900 6900 50  0001 C CNN
F 4 "C81598" H 7900 6900 50  0001 C CNN "JLC PCB Part Number"
	1    7900 6900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7950 6650 7900 6650
Wire Wire Line
	7900 6650 7900 6750
Connection ~ 8550 6650
Connection ~ 7900 7050
$Comp
L keyboard_parts:KEYSW K_H1
U 1 1 000003B1
P 9250 6650
F 0 "K_H1" H 9250 6883 60  0000 C CNN
F 1 "KEYSW" H 9250 6550 60  0001 C CNN
F 2 "keyswitches:Kailh_socket_MX_1u" H 9250 6650 60  0001 C CNN
F 3 "" H 9250 6650 60  0000 C CNN
	1    9250 6650
	1    0    0    -1  
$EndComp
$Comp
L Device:D D_H1
U 1 1 000003B0
P 8900 6900
F 0 "D_H1" V 8946 6821 50  0000 R CNN
F 1 "D" V 8845 6821 50  0000 R CNN
F 2 "keyboard_parts:D_SOD123_axial" H 8900 6900 50  0001 C CNN
F 3 "~" H 8900 6900 50  0001 C CNN
F 4 "C81598" H 8900 6900 50  0001 C CNN "JLC PCB Part Number"
	1    8900 6900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8950 6650 8900 6650
Wire Wire Line
	8900 6650 8900 6750
Connection ~ 9550 6650
Connection ~ 8900 7050
$Comp
L keyboard_parts:KEYSW K_J1
U 1 1 000003C1
P 10250 6650
F 0 "K_J1" H 10250 6883 60  0000 C CNN
F 1 "KEYSW" H 10250 6550 60  0001 C CNN
F 2 "keyswitches:Kailh_socket_MX_1u" H 10250 6650 60  0001 C CNN
F 3 "" H 10250 6650 60  0000 C CNN
	1    10250 6650
	1    0    0    -1  
$EndComp
$Comp
L Device:D D_J1
U 1 1 000003C0
P 9900 6900
F 0 "D_J1" V 9946 6821 50  0000 R CNN
F 1 "D" V 9845 6821 50  0000 R CNN
F 2 "keyboard_parts:D_SOD123_axial" H 9900 6900 50  0001 C CNN
F 3 "~" H 9900 6900 50  0001 C CNN
F 4 "C81598" H 9900 6900 50  0001 C CNN "JLC PCB Part Number"
	1    9900 6900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	9950 6650 9900 6650
Wire Wire Line
	9900 6650 9900 6750
Connection ~ 10550 6650
Connection ~ 9900 7050
$Comp
L keyboard_parts:KEYSW K_K1
U 1 1 000003D1
P 11250 6650
F 0 "K_K1" H 11250 6883 60  0000 C CNN
F 1 "KEYSW" H 11250 6550 60  0001 C CNN
F 2 "keyswitches:Kailh_socket_MX_1u" H 11250 6650 60  0001 C CNN
F 3 "" H 11250 6650 60  0000 C CNN
	1    11250 6650
	1    0    0    -1  
$EndComp
$Comp
L Device:D D_K1
U 1 1 000003D0
P 10900 6900
F 0 "D_K1" V 10946 6821 50  0000 R CNN
F 1 "D" V 10845 6821 50  0000 R CNN
F 2 "keyboard_parts:D_SOD123_axial" H 10900 6900 50  0001 C CNN
F 3 "~" H 10900 6900 50  0001 C CNN
F 4 "C81598" H 10900 6900 50  0001 C CNN "JLC PCB Part Number"
	1    10900 6900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	10950 6650 10900 6650
Wire Wire Line
	10900 6650 10900 6750
Connection ~ 11550 6650
Connection ~ 10900 7050
$Comp
L keyboard_parts:KEYSW K_L1
U 1 1 000003E1
P 12250 6650
F 0 "K_L1" H 12250 6883 60  0000 C CNN
F 1 "KEYSW" H 12250 6550 60  0001 C CNN
F 2 "keyswitches:Kailh_socket_MX_1u" H 12250 6650 60  0001 C CNN
F 3 "" H 12250 6650 60  0000 C CNN
	1    12250 6650
	1    0    0    -1  
$EndComp
$Comp
L Device:D D_L1
U 1 1 000003E0
P 11900 6900
F 0 "D_L1" V 11946 6821 50  0000 R CNN
F 1 "D" V 11845 6821 50  0000 R CNN
F 2 "keyboard_parts:D_SOD123_axial" H 11900 6900 50  0001 C CNN
F 3 "~" H 11900 6900 50  0001 C CNN
F 4 "C81598" H 11900 6900 50  0001 C CNN "JLC PCB Part Number"
	1    11900 6900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	11950 6650 11900 6650
Wire Wire Line
	11900 6650 11900 6750
Connection ~ 12550 6650
Connection ~ 11900 7050
$Comp
L keyboard_parts:KEYSW K_SemiCol1
U 1 1 000003F1
P 13250 6650
F 0 "K_SemiCol1" H 13250 6883 60  0000 C CNN
F 1 "KEYSW" H 13250 6550 60  0001 C CNN
F 2 "keyswitches:Kailh_socket_MX_1u" H 13250 6650 60  0001 C CNN
F 3 "" H 13250 6650 60  0000 C CNN
	1    13250 6650
	1    0    0    -1  
$EndComp
$Comp
L Device:D D_SemiCol1
U 1 1 000003F0
P 12900 6900
F 0 "D_SemiCol1" V 12946 6821 50  0000 R CNN
F 1 "D" V 12845 6821 50  0000 R CNN
F 2 "keyboard_parts:D_SOD123_axial" H 12900 6900 50  0001 C CNN
F 3 "~" H 12900 6900 50  0001 C CNN
F 4 "C81598" H 12900 6900 50  0001 C CNN "JLC PCB Part Number"
	1    12900 6900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	12950 6650 12900 6650
Wire Wire Line
	12900 6650 12900 6750
Connection ~ 13550 6650
Connection ~ 12900 7050
$Comp
L keyboard_parts:KEYSW K_Quote1
U 1 1 00000401
P 14250 6650
F 0 "K_Quote1" H 14250 6883 60  0000 C CNN
F 1 "KEYSW" H 14250 6550 60  0001 C CNN
F 2 "keyswitches:Kailh_socket_MX_1u" H 14250 6650 60  0001 C CNN
F 3 "" H 14250 6650 60  0000 C CNN
	1    14250 6650
	1    0    0    -1  
$EndComp
$Comp
L Device:D D_Quote1
U 1 1 00000400
P 13900 6900
F 0 "D_Quote1" V 13946 6821 50  0000 R CNN
F 1 "D" V 13845 6821 50  0000 R CNN
F 2 "keyboard_parts:D_SOD123_axial" H 13900 6900 50  0001 C CNN
F 3 "~" H 13900 6900 50  0001 C CNN
F 4 "C81598" H 13900 6900 50  0001 C CNN "JLC PCB Part Number"
	1    13900 6900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	13950 6650 13900 6650
Wire Wire Line
	13900 6650 13900 6750
Connection ~ 13900 7050
$Comp
L keyboard_parts:KEYSW K_Enter1
U 1 1 00000411
P 15250 6650
F 0 "K_Enter1" H 15250 6883 60  0000 C CNN
F 1 "KEYSW" H 15250 6550 60  0001 C CNN
F 2 "keyswitches:Kailh_socket_MX_2.25u" H 15250 6650 60  0001 C CNN
F 3 "" H 15250 6650 60  0000 C CNN
	1    15250 6650
	1    0    0    -1  
$EndComp
$Comp
L Device:D D_Enter1
U 1 1 00000410
P 14900 6900
F 0 "D_Enter1" V 14946 6821 50  0000 R CNN
F 1 "D" V 14845 6821 50  0000 R CNN
F 2 "keyboard_parts:D_SOD123_axial" H 14900 6900 50  0001 C CNN
F 3 "~" H 14900 6900 50  0001 C CNN
F 4 "C81598" H 14900 6900 50  0001 C CNN "JLC PCB Part Number"
	1    14900 6900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	14950 6650 14900 6650
Wire Wire Line
	14900 6650 14900 6750
Connection ~ 15550 6650
Wire Wire Line
	1900 7050 3900 7050
Text GLabel 1650 8050 0    50   Input ~ 0
row5
$Comp
L keyboard_parts:KEYSW K_LftShift1
U 1 1 00000421
P 3250 7650
F 0 "K_LftShift1" H 3250 7883 60  0000 C CNN
F 1 "KEYSW" H 3250 7550 60  0001 C CNN
F 2 "keyswitches:Kailh_socket_MX_2.25u" H 3250 7650 60  0001 C CNN
F 3 "" H 3250 7650 60  0000 C CNN
	1    3250 7650
	1    0    0    -1  
$EndComp
$Comp
L Device:D D_LftShift1
U 1 1 00000420
P 2900 7900
F 0 "D_LftShift1" V 2946 7821 50  0000 R CNN
F 1 "D" V 2845 7821 50  0000 R CNN
F 2 "keyboard_parts:D_SOD123_axial" H 2900 7900 50  0001 C CNN
F 3 "~" H 2900 7900 50  0001 C CNN
F 4 "C81598" H 2900 7900 50  0001 C CNN "JLC PCB Part Number"
	1    2900 7900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2950 7650 2900 7650
Wire Wire Line
	2900 7650 2900 7750
Connection ~ 3550 7650
Connection ~ 2900 8050
$Comp
L keyboard_parts:KEYSW K_Z1
U 1 1 00000431
P 4250 7650
F 0 "K_Z1" H 4250 7883 60  0000 C CNN
F 1 "KEYSW" H 4250 7550 60  0001 C CNN
F 2 "keyswitches:Kailh_socket_MX_1u" H 4250 7650 60  0001 C CNN
F 3 "" H 4250 7650 60  0000 C CNN
	1    4250 7650
	1    0    0    -1  
$EndComp
$Comp
L Device:D D_Z1
U 1 1 00000430
P 3900 7900
F 0 "D_Z1" V 3946 7821 50  0000 R CNN
F 1 "D" V 3845 7821 50  0000 R CNN
F 2 "keyboard_parts:D_SOD123_axial" H 3900 7900 50  0001 C CNN
F 3 "~" H 3900 7900 50  0001 C CNN
F 4 "C81598" H 3900 7900 50  0001 C CNN "JLC PCB Part Number"
	1    3900 7900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3950 7650 3900 7650
Wire Wire Line
	3900 7650 3900 7750
Connection ~ 3900 8050
$Comp
L keyboard_parts:KEYSW K_X1
U 1 1 00000441
P 5250 7650
F 0 "K_X1" H 5250 7883 60  0000 C CNN
F 1 "KEYSW" H 5250 7550 60  0001 C CNN
F 2 "keyswitches:Kailh_socket_MX_1u" H 5250 7650 60  0001 C CNN
F 3 "" H 5250 7650 60  0000 C CNN
	1    5250 7650
	1    0    0    -1  
$EndComp
$Comp
L Device:D D_X1
U 1 1 00000440
P 4900 7900
F 0 "D_X1" V 4946 7821 50  0000 R CNN
F 1 "D" V 4845 7821 50  0000 R CNN
F 2 "keyboard_parts:D_SOD123_axial" H 4900 7900 50  0001 C CNN
F 3 "~" H 4900 7900 50  0001 C CNN
F 4 "C81598" H 4900 7900 50  0001 C CNN "JLC PCB Part Number"
	1    4900 7900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4950 7650 4900 7650
Wire Wire Line
	4900 7650 4900 7750
Connection ~ 5550 7650
Connection ~ 4900 8050
$Comp
L keyboard_parts:KEYSW K_C1
U 1 1 00000451
P 6250 7650
F 0 "K_C1" H 6250 7883 60  0000 C CNN
F 1 "KEYSW" H 6250 7550 60  0001 C CNN
F 2 "keyswitches:Kailh_socket_MX_1u" H 6250 7650 60  0001 C CNN
F 3 "" H 6250 7650 60  0000 C CNN
	1    6250 7650
	1    0    0    -1  
$EndComp
$Comp
L Device:D D_C1
U 1 1 00000450
P 5900 7900
F 0 "D_C1" V 5946 7821 50  0000 R CNN
F 1 "D" V 5845 7821 50  0000 R CNN
F 2 "keyboard_parts:D_SOD123_axial" H 5900 7900 50  0001 C CNN
F 3 "~" H 5900 7900 50  0001 C CNN
F 4 "C81598" H 5900 7900 50  0001 C CNN "JLC PCB Part Number"
	1    5900 7900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5950 7650 5900 7650
Wire Wire Line
	5900 7650 5900 7750
Connection ~ 5900 8050
$Comp
L keyboard_parts:KEYSW K_V1
U 1 1 00000461
P 7250 7650
F 0 "K_V1" H 7250 7883 60  0000 C CNN
F 1 "KEYSW" H 7250 7550 60  0001 C CNN
F 2 "keyswitches:Kailh_socket_MX_1u" H 7250 7650 60  0001 C CNN
F 3 "" H 7250 7650 60  0000 C CNN
	1    7250 7650
	1    0    0    -1  
$EndComp
$Comp
L Device:D D_V1
U 1 1 00000460
P 6900 7900
F 0 "D_V1" V 6946 7821 50  0000 R CNN
F 1 "D" V 6845 7821 50  0000 R CNN
F 2 "keyboard_parts:D_SOD123_axial" H 6900 7900 50  0001 C CNN
F 3 "~" H 6900 7900 50  0001 C CNN
F 4 "C81598" H 6900 7900 50  0001 C CNN "JLC PCB Part Number"
	1    6900 7900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6950 7650 6900 7650
Wire Wire Line
	6900 7650 6900 7750
Connection ~ 6900 8050
$Comp
L keyboard_parts:KEYSW K_B1
U 1 1 00000471
P 8250 7650
F 0 "K_B1" H 8250 7883 60  0000 C CNN
F 1 "KEYSW" H 8250 7550 60  0001 C CNN
F 2 "keyswitches:Kailh_socket_MX_1u" H 8250 7650 60  0001 C CNN
F 3 "" H 8250 7650 60  0000 C CNN
	1    8250 7650
	1    0    0    -1  
$EndComp
$Comp
L Device:D D_B1
U 1 1 00000470
P 7900 7900
F 0 "D_B1" V 7946 7821 50  0000 R CNN
F 1 "D" V 7845 7821 50  0000 R CNN
F 2 "keyboard_parts:D_SOD123_axial" H 7900 7900 50  0001 C CNN
F 3 "~" H 7900 7900 50  0001 C CNN
F 4 "C81598" H 7900 7900 50  0001 C CNN "JLC PCB Part Number"
	1    7900 7900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7950 7650 7900 7650
Wire Wire Line
	7900 7650 7900 7750
Connection ~ 8550 7650
Connection ~ 7900 8050
$Comp
L keyboard_parts:KEYSW K_N1
U 1 1 00000481
P 9250 7650
F 0 "K_N1" H 9250 7883 60  0000 C CNN
F 1 "KEYSW" H 9250 7550 60  0001 C CNN
F 2 "keyswitches:Kailh_socket_MX_1u" H 9250 7650 60  0001 C CNN
F 3 "" H 9250 7650 60  0000 C CNN
	1    9250 7650
	1    0    0    -1  
$EndComp
$Comp
L Device:D D_N1
U 1 1 00000480
P 8900 7900
F 0 "D_N1" V 8946 7821 50  0000 R CNN
F 1 "D" V 8845 7821 50  0000 R CNN
F 2 "keyboard_parts:D_SOD123_axial" H 8900 7900 50  0001 C CNN
F 3 "~" H 8900 7900 50  0001 C CNN
F 4 "C81598" H 8900 7900 50  0001 C CNN "JLC PCB Part Number"
	1    8900 7900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8950 7650 8900 7650
Wire Wire Line
	8900 7650 8900 7750
Connection ~ 8900 8050
$Comp
L keyboard_parts:KEYSW K_M1
U 1 1 00000491
P 10250 7650
F 0 "K_M1" H 10250 7883 60  0000 C CNN
F 1 "KEYSW" H 10250 7550 60  0001 C CNN
F 2 "keyswitches:Kailh_socket_MX_1u" H 10250 7650 60  0001 C CNN
F 3 "" H 10250 7650 60  0000 C CNN
	1    10250 7650
	1    0    0    -1  
$EndComp
$Comp
L Device:D D_M1
U 1 1 00000490
P 9900 7900
F 0 "D_M1" V 9946 7821 50  0000 R CNN
F 1 "D" V 9845 7821 50  0000 R CNN
F 2 "keyboard_parts:D_SOD123_axial" H 9900 7900 50  0001 C CNN
F 3 "~" H 9900 7900 50  0001 C CNN
F 4 "C81598" H 9900 7900 50  0001 C CNN "JLC PCB Part Number"
	1    9900 7900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	9950 7650 9900 7650
Wire Wire Line
	9900 7650 9900 7750
Connection ~ 9900 8050
$Comp
L keyboard_parts:KEYSW K_Comma1
U 1 1 000004A1
P 11250 7650
F 0 "K_Comma1" H 11250 7883 60  0000 C CNN
F 1 "KEYSW" H 11250 7550 60  0001 C CNN
F 2 "keyswitches:Kailh_socket_MX_1u" H 11250 7650 60  0001 C CNN
F 3 "" H 11250 7650 60  0000 C CNN
	1    11250 7650
	1    0    0    -1  
$EndComp
$Comp
L Device:D D_Comma1
U 1 1 000004A0
P 10900 7900
F 0 "D_Comma1" V 10946 7821 50  0000 R CNN
F 1 "D" V 10845 7821 50  0000 R CNN
F 2 "keyboard_parts:D_SOD123_axial" H 10900 7900 50  0001 C CNN
F 3 "~" H 10900 7900 50  0001 C CNN
F 4 "C81598" H 10900 7900 50  0001 C CNN "JLC PCB Part Number"
	1    10900 7900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	10950 7650 10900 7650
Wire Wire Line
	10900 7650 10900 7750
Connection ~ 10900 8050
$Comp
L keyboard_parts:KEYSW K_Point1
U 1 1 000004B1
P 12250 7650
F 0 "K_Point1" H 12250 7883 60  0000 C CNN
F 1 "KEYSW" H 12250 7550 60  0001 C CNN
F 2 "keyswitches:Kailh_socket_MX_1u" H 12250 7650 60  0001 C CNN
F 3 "" H 12250 7650 60  0000 C CNN
	1    12250 7650
	1    0    0    -1  
$EndComp
$Comp
L Device:D D_Point1
U 1 1 000004B0
P 11900 7900
F 0 "D_Point1" V 11946 7821 50  0000 R CNN
F 1 "D" V 11845 7821 50  0000 R CNN
F 2 "keyboard_parts:D_SOD123_axial" H 11900 7900 50  0001 C CNN
F 3 "~" H 11900 7900 50  0001 C CNN
F 4 "C81598" H 11900 7900 50  0001 C CNN "JLC PCB Part Number"
	1    11900 7900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	11950 7650 11900 7650
Wire Wire Line
	11900 7650 11900 7750
Connection ~ 12550 7650
Connection ~ 11900 8050
$Comp
L keyboard_parts:KEYSW K_FwdSlash1
U 1 1 000004C1
P 13250 7650
F 0 "K_FwdSlash1" H 13250 7883 60  0000 C CNN
F 1 "KEYSW" H 13250 7550 60  0001 C CNN
F 2 "keyswitches:Kailh_socket_MX_1u" H 13250 7650 60  0001 C CNN
F 3 "" H 13250 7650 60  0000 C CNN
	1    13250 7650
	1    0    0    -1  
$EndComp
$Comp
L Device:D D_FwdSlash1
U 1 1 000004C0
P 12900 7900
F 0 "D_FwdSlash1" V 12946 7821 50  0000 R CNN
F 1 "D" V 12845 7821 50  0000 R CNN
F 2 "keyboard_parts:D_SOD123_axial" H 12900 7900 50  0001 C CNN
F 3 "~" H 12900 7900 50  0001 C CNN
F 4 "C81598" H 12900 7900 50  0001 C CNN "JLC PCB Part Number"
	1    12900 7900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	12950 7650 12900 7650
Wire Wire Line
	12900 7650 12900 7750
Connection ~ 13550 7650
Connection ~ 12900 8050
$Comp
L keyboard_parts:KEYSW K_RgtShift1
U 1 1 000004D1
P 15250 7650
F 0 "K_RgtShift1" H 15250 7883 60  0000 C CNN
F 1 "KEYSW" H 15250 7550 60  0001 C CNN
F 2 "keyswitches:Kailh_socket_MX_2.75u" H 15250 7650 60  0001 C CNN
F 3 "" H 15250 7650 60  0000 C CNN
	1    15250 7650
	1    0    0    -1  
$EndComp
$Comp
L Device:D D_RgtShift1
U 1 1 000004D0
P 14900 7900
F 0 "D_RgtShift1" V 14946 7821 50  0000 R CNN
F 1 "D" V 14845 7821 50  0000 R CNN
F 2 "keyboard_parts:D_SOD123_axial" H 14900 7900 50  0001 C CNN
F 3 "~" H 14900 7900 50  0001 C CNN
F 4 "C81598" H 14900 7900 50  0001 C CNN "JLC PCB Part Number"
	1    14900 7900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	14950 7650 14900 7650
Wire Wire Line
	14900 7650 14900 7750
Connection ~ 15550 7650
Connection ~ 14900 8050
$Comp
L keyboard_parts:KEYSW K_Up1
U 1 1 000004E1
P 18250 7650
F 0 "K_Up1" H 18250 7883 60  0000 C CNN
F 1 "KEYSW" H 18250 7550 60  0001 C CNN
F 2 "keyswitches:Kailh_socket_MX_1u" H 18250 7650 60  0001 C CNN
F 3 "" H 18250 7650 60  0000 C CNN
	1    18250 7650
	1    0    0    -1  
$EndComp
$Comp
L Device:D D_Up1
U 1 1 000004E0
P 17900 7900
F 0 "D_Up1" V 17946 7821 50  0000 R CNN
F 1 "D" V 17845 7821 50  0000 R CNN
F 2 "keyboard_parts:D_SOD123_axial" H 17900 7900 50  0001 C CNN
F 3 "~" H 17900 7900 50  0001 C CNN
F 4 "C81598" H 17900 7900 50  0001 C CNN "JLC PCB Part Number"
	1    17900 7900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	17950 7650 17900 7650
Wire Wire Line
	17900 7650 17900 7750
Connection ~ 18550 7650
Text GLabel 1650 9050 0    50   Input ~ 0
row6
Wire Wire Line
	1650 9050 1900 9050
$Comp
L keyboard_parts:KEYSW K_LftCtrl1
U 1 1 000004F1
P 2250 8650
F 0 "K_LftCtrl1" H 2250 8883 60  0000 C CNN
F 1 "KEYSW" H 2250 8550 60  0001 C CNN
F 2 "keyswitches:Kailh_socket_MX_1.25u" H 2250 8650 60  0001 C CNN
F 3 "" H 2250 8650 60  0000 C CNN
	1    2250 8650
	1    0    0    -1  
$EndComp
$Comp
L Device:D D_LftCtrl1
U 1 1 000004F0
P 1900 8900
F 0 "D_LftCtrl1" V 1946 8821 50  0000 R CNN
F 1 "D" V 1845 8821 50  0000 R CNN
F 2 "keyboard_parts:D_SOD123_axial" H 1900 8900 50  0001 C CNN
F 3 "~" H 1900 8900 50  0001 C CNN
F 4 "C81598" H 1900 8900 50  0001 C CNN "JLC PCB Part Number"
	1    1900 8900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1950 8650 1900 8650
Wire Wire Line
	1900 8650 1900 8750
Connection ~ 1900 9050
$Comp
L keyboard_parts:KEYSW K_LftWin1
U 1 1 00000501
P 3250 8650
F 0 "K_LftWin1" H 3250 8883 60  0000 C CNN
F 1 "KEYSW" H 3250 8550 60  0001 C CNN
F 2 "keyswitches:Kailh_socket_MX_1.25u" H 3250 8650 60  0001 C CNN
F 3 "" H 3250 8650 60  0000 C CNN
	1    3250 8650
	1    0    0    -1  
$EndComp
$Comp
L Device:D D_LftWin1
U 1 1 00000500
P 2900 8900
F 0 "D_LftWin1" V 2946 8821 50  0000 R CNN
F 1 "D" V 2845 8821 50  0000 R CNN
F 2 "keyboard_parts:D_SOD123_axial" H 2900 8900 50  0001 C CNN
F 3 "~" H 2900 8900 50  0001 C CNN
F 4 "C81598" H 2900 8900 50  0001 C CNN "JLC PCB Part Number"
	1    2900 8900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2950 8650 2900 8650
Wire Wire Line
	2900 8650 2900 8750
Connection ~ 2900 9050
$Comp
L keyboard_parts:KEYSW K_LftAlt1
U 1 1 00000511
P 5250 8650
F 0 "K_LftAlt1" H 5250 8883 60  0000 C CNN
F 1 "KEYSW" H 5250 8550 60  0001 C CNN
F 2 "keyswitches:Kailh_socket_MX_1.25u" H 5250 8650 60  0001 C CNN
F 3 "" H 5250 8650 60  0000 C CNN
	1    5250 8650
	1    0    0    -1  
$EndComp
$Comp
L Device:D D_LftAlt1
U 1 1 00000510
P 4900 8900
F 0 "D_LftAlt1" V 4946 8821 50  0000 R CNN
F 1 "D" V 4845 8821 50  0000 R CNN
F 2 "keyboard_parts:D_SOD123_axial" H 4900 8900 50  0001 C CNN
F 3 "~" H 4900 8900 50  0001 C CNN
F 4 "C81598" H 4900 8900 50  0001 C CNN "JLC PCB Part Number"
	1    4900 8900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4950 8650 4900 8650
Wire Wire Line
	4900 8650 4900 8750
Connection ~ 4900 9050
$Comp
L keyboard_parts:KEYSW K_RgtAlt1
U 1 1 00000531
P 12250 8650
F 0 "K_RgtAlt1" H 12250 8883 60  0000 C CNN
F 1 "KEYSW" H 12250 8550 60  0001 C CNN
F 2 "keyswitches:Kailh_socket_MX_1.25u" H 12250 8650 60  0001 C CNN
F 3 "" H 12250 8650 60  0000 C CNN
	1    12250 8650
	1    0    0    -1  
$EndComp
$Comp
L Device:D D_RgtAlt1
U 1 1 00000530
P 11900 8900
F 0 "D_RgtAlt1" V 11946 8821 50  0000 R CNN
F 1 "D" V 11845 8821 50  0000 R CNN
F 2 "keyboard_parts:D_SOD123_axial" H 11900 8900 50  0001 C CNN
F 3 "~" H 11900 8900 50  0001 C CNN
F 4 "C81598" H 11900 8900 50  0001 C CNN "JLC PCB Part Number"
	1    11900 8900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	11950 8650 11900 8650
Wire Wire Line
	11900 8650 11900 8750
$Comp
L keyboard_parts:KEYSW K_Fn1
U 1 1 00000541
P 13250 8650
F 0 "K_Fn1" H 13250 8883 60  0000 C CNN
F 1 "KEYSW" H 13250 8550 60  0001 C CNN
F 2 "keyswitches:Kailh_socket_MX_1.25u" H 13250 8650 60  0001 C CNN
F 3 "" H 13250 8650 60  0000 C CNN
	1    13250 8650
	1    0    0    -1  
$EndComp
$Comp
L Device:D D_Fn1
U 1 1 00000540
P 12900 8900
F 0 "D_Fn1" V 12946 8821 50  0000 R CNN
F 1 "D" V 12845 8821 50  0000 R CNN
F 2 "keyboard_parts:D_SOD123_axial" H 12900 8900 50  0001 C CNN
F 3 "~" H 12900 8900 50  0001 C CNN
F 4 "C81598" H 12900 8900 50  0001 C CNN "JLC PCB Part Number"
	1    12900 8900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	12950 8650 12900 8650
Wire Wire Line
	12900 8650 12900 8750
Connection ~ 12900 9050
$Comp
L keyboard_parts:KEYSW K_RgtWin1
U 1 1 00000551
P 15250 8650
F 0 "K_RgtWin1" H 15250 8883 60  0000 C CNN
F 1 "KEYSW" H 15250 8550 60  0001 C CNN
F 2 "keyswitches:Kailh_socket_MX_1.25u" H 15250 8650 60  0001 C CNN
F 3 "" H 15250 8650 60  0000 C CNN
	1    15250 8650
	1    0    0    -1  
$EndComp
$Comp
L Device:D D_RgtWin1
U 1 1 00000550
P 14900 8900
F 0 "D_RgtWin1" V 14946 8821 50  0000 R CNN
F 1 "D" V 14845 8821 50  0000 R CNN
F 2 "keyboard_parts:D_SOD123_axial" H 14900 8900 50  0001 C CNN
F 3 "~" H 14900 8900 50  0001 C CNN
F 4 "C81598" H 14900 8900 50  0001 C CNN "JLC PCB Part Number"
	1    14900 8900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	14950 8650 14900 8650
Wire Wire Line
	14900 8650 14900 8750
Connection ~ 14900 9050
$Comp
L keyboard_parts:KEYSW K_RgtCtrl1
U 1 1 00000561
P 16250 8650
F 0 "K_RgtCtrl1" H 16250 8883 60  0000 C CNN
F 1 "KEYSW" H 16250 8550 60  0001 C CNN
F 2 "keyswitches:Kailh_socket_MX_1.25u" H 16250 8650 60  0001 C CNN
F 3 "" H 16250 8650 60  0000 C CNN
	1    16250 8650
	1    0    0    -1  
$EndComp
$Comp
L Device:D D_RgtCtrl1
U 1 1 00000560
P 15900 8900
F 0 "D_RgtCtrl1" V 15946 8821 50  0000 R CNN
F 1 "D" V 15845 8821 50  0000 R CNN
F 2 "keyboard_parts:D_SOD123_axial" H 15900 8900 50  0001 C CNN
F 3 "~" H 15900 8900 50  0001 C CNN
F 4 "C81598" H 15900 8900 50  0001 C CNN "JLC PCB Part Number"
	1    15900 8900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	15950 8650 15900 8650
Wire Wire Line
	15900 8650 15900 8750
Connection ~ 15900 9050
$Comp
L keyboard_parts:KEYSW K_Left1
U 1 1 00000571
P 17250 8650
F 0 "K_Left1" H 17250 8883 60  0000 C CNN
F 1 "KEYSW" H 17250 8550 60  0001 C CNN
F 2 "keyswitches:Kailh_socket_MX_1u" H 17250 8650 60  0001 C CNN
F 3 "" H 17250 8650 60  0000 C CNN
	1    17250 8650
	1    0    0    -1  
$EndComp
$Comp
L Device:D D_Left1
U 1 1 00000570
P 16900 8900
F 0 "D_Left1" V 16946 8821 50  0000 R CNN
F 1 "D" V 16845 8821 50  0000 R CNN
F 2 "keyboard_parts:D_SOD123_axial" H 16900 8900 50  0001 C CNN
F 3 "~" H 16900 8900 50  0001 C CNN
F 4 "C81598" H 16900 8900 50  0001 C CNN "JLC PCB Part Number"
	1    16900 8900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	16950 8650 16900 8650
Wire Wire Line
	16900 8650 16900 8750
Connection ~ 16900 9050
$Comp
L keyboard_parts:KEYSW K_Down1
U 1 1 00000581
P 18250 8650
F 0 "K_Down1" H 18250 8883 60  0000 C CNN
F 1 "KEYSW" H 18250 8550 60  0001 C CNN
F 2 "keyswitches:Kailh_socket_MX_1u" H 18250 8650 60  0001 C CNN
F 3 "" H 18250 8650 60  0000 C CNN
	1    18250 8650
	1    0    0    -1  
$EndComp
$Comp
L Device:D D_Down1
U 1 1 00000580
P 17900 8900
F 0 "D_Down1" V 17946 8821 50  0000 R CNN
F 1 "D" V 17845 8821 50  0000 R CNN
F 2 "keyboard_parts:D_SOD123_axial" H 17900 8900 50  0001 C CNN
F 3 "~" H 17900 8900 50  0001 C CNN
F 4 "C81598" H 17900 8900 50  0001 C CNN "JLC PCB Part Number"
	1    17900 8900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	17950 8650 17900 8650
Wire Wire Line
	17900 8650 17900 8750
Connection ~ 17900 9050
$Comp
L keyboard_parts:KEYSW K_Right1
U 1 1 00000591
P 19250 8650
F 0 "K_Right1" H 19250 8883 60  0000 C CNN
F 1 "KEYSW" H 19250 8550 60  0001 C CNN
F 2 "keyswitches:Kailh_socket_MX_1u" H 19250 8650 60  0001 C CNN
F 3 "" H 19250 8650 60  0000 C CNN
	1    19250 8650
	1    0    0    -1  
$EndComp
$Comp
L Device:D D_Right1
U 1 1 00000590
P 18900 8900
F 0 "D_Right1" V 18946 8821 50  0000 R CNN
F 1 "D" V 18845 8821 50  0000 R CNN
F 2 "keyboard_parts:D_SOD123_axial" H 18900 8900 50  0001 C CNN
F 3 "~" H 18900 8900 50  0001 C CNN
F 4 "C81598" H 18900 8900 50  0001 C CNN "JLC PCB Part Number"
	1    18900 8900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	18950 8650 18900 8650
Wire Wire Line
	18900 8650 18900 8750
Wire Wire Line
	1900 9050 2900 9050
Wire Wire Line
	2550 8650 2550 6650
Wire Wire Line
	3550 8650 3550 7650
Wire Wire Line
	4550 7650 4550 6650
Wire Wire Line
	5550 8650 5550 7650
Wire Wire Line
	6550 7650 6550 6650
Wire Wire Line
	7550 7650 7550 6650
Wire Wire Line
	8550 8650 8550 7650
Wire Wire Line
	9550 7650 9550 6650
Wire Wire Line
	10550 7650 10550 6650
Wire Wire Line
	11550 7650 11550 6650
Wire Wire Line
	12550 8650 12550 7650
Wire Wire Line
	13550 8650 13550 7650
Wire Wire Line
	14550 6650 14550 5650
Wire Wire Line
	15550 8650 15550 7650
Wire Wire Line
	16550 8650 16550 5650
Wire Wire Line
	17550 8650 17550 5650
Wire Wire Line
	18550 8650 18550 7650
Wire Wire Line
	19550 8650 19550 5650
Wire Wire Line
	3900 3050 4900 3050
Wire Wire Line
	4900 3050 5900 3050
Wire Wire Line
	5900 3050 6900 3050
Wire Wire Line
	6900 3050 8900 3050
Wire Wire Line
	8900 3050 9900 3050
Wire Wire Line
	9900 3050 10900 3050
Wire Wire Line
	10900 3050 11900 3050
Wire Wire Line
	11900 3050 12900 3050
Wire Wire Line
	12900 3050 13900 3050
Wire Wire Line
	13900 3050 14900 3050
Wire Wire Line
	14900 3050 15900 3050
Wire Wire Line
	15900 3050 16900 3050
Wire Wire Line
	16900 3050 17900 3050
Wire Wire Line
	17900 3050 18900 3050
Wire Wire Line
	17550 3650 17550 2650
Wire Wire Line
	16900 4050 17900 4050
Wire Wire Line
	18550 3650 18550 2650
Wire Wire Line
	17900 4050 18900 4050
Wire Wire Line
	19550 3650 19550 2650
Wire Wire Line
	2550 4650 2550 2650
Wire Wire Line
	2900 5050 3900 5050
Wire Wire Line
	4550 4650 4550 2650
Wire Wire Line
	3900 5050 4900 5050
Wire Wire Line
	5550 4650 5550 2650
Wire Wire Line
	4900 5050 5900 5050
Wire Wire Line
	6550 4650 6550 2650
Wire Wire Line
	5900 5050 6900 5050
Wire Wire Line
	7550 4650 7550 2650
Wire Wire Line
	6900 5050 7900 5050
Wire Wire Line
	7900 5050 8900 5050
Wire Wire Line
	9550 4650 9550 2650
Wire Wire Line
	8900 5050 9900 5050
Wire Wire Line
	10550 4650 10550 2650
Wire Wire Line
	9900 5050 10900 5050
Wire Wire Line
	11550 4650 11550 2650
Wire Wire Line
	10900 5050 11900 5050
Wire Wire Line
	12550 4650 12550 2650
Wire Wire Line
	11900 5050 12900 5050
Wire Wire Line
	13550 4650 13550 2650
Wire Wire Line
	12900 5050 13900 5050
Wire Wire Line
	14550 4650 14550 2650
Wire Wire Line
	13900 5050 15900 5050
Wire Wire Line
	16550 4650 16550 2650
Wire Wire Line
	15900 5050 16900 5050
Wire Wire Line
	17550 4650 17550 3650
Wire Wire Line
	16900 5050 17900 5050
Wire Wire Line
	18550 4650 18550 3650
Wire Wire Line
	17900 5050 18900 5050
Wire Wire Line
	19550 4650 19550 3650
Wire Wire Line
	2550 5650 2550 4650
Wire Wire Line
	4550 5650 4550 4650
Wire Wire Line
	3900 6050 4900 6050
Wire Wire Line
	5550 5650 5550 4650
Wire Wire Line
	4900 6050 5900 6050
Wire Wire Line
	6550 5650 6550 4650
Wire Wire Line
	5900 6050 6900 6050
Wire Wire Line
	7550 5650 7550 4650
Wire Wire Line
	6900 6050 7900 6050
Wire Wire Line
	8550 5650 8550 4650
Wire Wire Line
	7900 6050 8900 6050
Wire Wire Line
	9550 5650 9550 4650
Wire Wire Line
	8900 6050 9900 6050
Wire Wire Line
	10550 5650 10550 4650
Wire Wire Line
	9900 6050 10900 6050
Wire Wire Line
	11550 5650 11550 4650
Wire Wire Line
	10900 6050 11900 6050
Wire Wire Line
	12550 5650 12550 4650
Wire Wire Line
	11900 6050 12900 6050
Wire Wire Line
	13550 5650 13550 4650
Wire Wire Line
	12900 6050 13900 6050
Wire Wire Line
	14550 5650 14550 4650
Wire Wire Line
	13900 6050 14900 6050
Wire Wire Line
	15550 5650 15550 2650
Wire Wire Line
	14900 6050 15900 6050
Wire Wire Line
	16550 5650 16550 4650
Wire Wire Line
	15900 6050 16900 6050
Wire Wire Line
	17550 5650 17550 4650
Wire Wire Line
	16900 6050 17900 6050
Wire Wire Line
	18550 5650 18550 4650
Wire Wire Line
	17900 6050 18900 6050
Wire Wire Line
	19550 5650 19550 4650
Wire Wire Line
	2550 6650 2550 5650
Wire Wire Line
	4550 6650 4550 5650
Wire Wire Line
	3900 7050 4900 7050
Wire Wire Line
	5550 6650 5550 5650
Wire Wire Line
	4900 7050 5900 7050
Wire Wire Line
	6550 6650 6550 5650
Wire Wire Line
	5900 7050 6900 7050
Wire Wire Line
	7550 6650 7550 5650
Wire Wire Line
	6900 7050 7900 7050
Wire Wire Line
	8550 6650 8550 5650
Wire Wire Line
	7900 7050 8900 7050
Wire Wire Line
	9550 6650 9550 5650
Wire Wire Line
	8900 7050 9900 7050
Wire Wire Line
	10550 6650 10550 5650
Wire Wire Line
	9900 7050 10900 7050
Wire Wire Line
	11550 6650 11550 5650
Wire Wire Line
	10900 7050 11900 7050
Wire Wire Line
	12550 6650 12550 5650
Wire Wire Line
	11900 7050 12900 7050
Wire Wire Line
	13550 6650 13550 5650
Wire Wire Line
	12900 7050 13900 7050
Wire Wire Line
	13900 7050 14900 7050
Wire Wire Line
	15550 6650 15550 5650
Wire Wire Line
	3550 7650 3550 4650
Wire Wire Line
	2900 8050 3900 8050
Wire Wire Line
	3900 8050 4900 8050
Wire Wire Line
	5550 7650 5550 6650
Wire Wire Line
	4900 8050 5900 8050
Wire Wire Line
	5900 8050 6900 8050
Wire Wire Line
	6900 8050 7900 8050
Wire Wire Line
	8550 7650 8550 6650
Wire Wire Line
	7900 8050 8900 8050
Wire Wire Line
	8900 8050 9900 8050
Wire Wire Line
	9900 8050 10900 8050
Wire Wire Line
	10900 8050 11900 8050
Wire Wire Line
	12550 7650 12550 6650
Wire Wire Line
	11900 8050 12900 8050
Wire Wire Line
	13550 7650 13550 6650
Wire Wire Line
	12900 8050 14900 8050
Wire Wire Line
	15550 7650 15550 6650
Wire Wire Line
	14900 8050 17900 8050
Wire Wire Line
	18550 7650 18550 5650
Wire Wire Line
	2900 9050 4900 9050
Wire Wire Line
	11900 9050 12900 9050
Wire Wire Line
	12900 9050 14900 9050
Wire Wire Line
	14900 9050 15900 9050
Wire Wire Line
	15900 9050 16900 9050
Wire Wire Line
	16900 9050 17900 9050
Wire Wire Line
	17900 9050 18900 9050
Wire Wire Line
	3550 2300 3550 4650
Wire Wire Line
	8550 2300 8550 4650
Wire Wire Line
	1650 4050 16900 4050
Wire Wire Line
	1650 8050 2900 8050
Connection ~ 11900 9050
Text GLabel 3600 13500 0    50   Input ~ 0
col17
Text GLabel 3600 13600 0    50   Input ~ 0
col16
Text GLabel 3600 13700 0    50   Input ~ 0
col15
Text GLabel 5750 13500 2    50   Input ~ 0
col14
Text GLabel 5750 13400 2    50   Input ~ 0
col13
Text GLabel 5750 13000 2    50   Input ~ 0
col12
Text GLabel 5750 13300 2    50   Input ~ 0
col11
Text GLabel 5750 13200 2    50   Input ~ 0
col10
Text GLabel 5750 13100 2    50   Input ~ 0
col9
Text GLabel 5750 12800 2    50   Input ~ 0
col7
Text GLabel 5750 12000 2    50   Input ~ 0
col2
Text GLabel 5750 11900 2    50   Input ~ 0
col1
Text GLabel 3600 13400 0    50   Input ~ 0
row6
Text GLabel 3600 13300 0    50   Input ~ 0
row5
Text GLabel 3600 12700 0    50   Input ~ 0
row4
$Comp
L keyboard_parts:KEYSW K_Space1
U 1 1 612CBB99
P 8250 8650
F 0 "K_Space1" H 8250 8883 60  0000 C CNN
F 1 "KEYSW" H 8250 8550 60  0001 C CNN
F 2 "keyswitches:Kailh_socket_MX_6.25u" H 8250 8650 60  0001 C CNN
F 3 "" H 8250 8650 60  0000 C CNN
	1    8250 8650
	1    0    0    -1  
$EndComp
$Comp
L Device:D D_Space1
U 1 1 612CBB9F
P 7900 8900
F 0 "D_Space1" V 7946 8821 50  0000 R CNN
F 1 "D" V 7845 8821 50  0000 R CNN
F 2 "keyboard_parts:D_SOD123_axial" H 7900 8900 50  0001 C CNN
F 3 "~" H 7900 8900 50  0001 C CNN
F 4 "C81598" H 7900 8900 50  0001 C CNN "JLC PCB Part Number"
	1    7900 8900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7950 8650 7900 8650
Wire Wire Line
	7900 8650 7900 8750
$Comp
L Device:C_Small C1
U 1 1 612FA52C
P 1950 13750
F 0 "C1" H 2042 13796 50  0000 L CNN
F 1 "22p" H 2042 13705 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.18x1.45mm_HandSolder" H 1950 13750 50  0001 C CNN
F 3 "~" H 1950 13750 50  0001 C CNN
F 4 "C1804" H 1950 13750 50  0001 C CNN "JLC PCB Part Number"
	1    1950 13750
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C2
U 1 1 612FA9C3
P 2650 13750
F 0 "C2" H 2742 13796 50  0000 L CNN
F 1 "22p" H 2742 13705 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.18x1.45mm_HandSolder" H 2650 13750 50  0001 C CNN
F 3 "~" H 2650 13750 50  0001 C CNN
F 4 "C1804" H 2650 13750 50  0001 C CNN "JLC PCB Part Number"
	1    2650 13750
	1    0    0    -1  
$EndComp
Wire Wire Line
	2000 13550 1950 13550
Wire Wire Line
	1950 13550 1950 13650
Wire Wire Line
	1950 13850 1950 13950
Wire Wire Line
	1950 13950 2300 13950
Wire Wire Line
	2650 13950 2650 13850
Wire Wire Line
	2650 13650 2650 13550
Wire Wire Line
	2650 13550 2600 13550
Wire Wire Line
	3600 13100 1950 13100
Wire Wire Line
	1950 13100 1950 13550
Connection ~ 1950 13550
Wire Wire Line
	3600 13200 2650 13200
Wire Wire Line
	2650 13200 2650 13550
Connection ~ 2650 13550
Wire Wire Line
	2300 13750 2300 13950
Connection ~ 2300 13950
Wire Wire Line
	2300 13950 2650 13950
Wire Wire Line
	2300 13950 2300 14000
$Comp
L Device:C_Small C4
U 1 1 614BBEE4
P 7350 12500
F 0 "C4" H 7442 12546 50  0000 L CNN
F 1 "0.1u" H 7442 12455 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.18x1.45mm_HandSolder" H 7350 12500 50  0001 C CNN
F 3 "~" H 7350 12500 50  0001 C CNN
F 4 "C49678" H 7350 12500 50  0001 C CNN "JLC PCB Part Number"
	1    7350 12500
	1    0    0    -1  
$EndComp
Wire Wire Line
	7350 12300 7350 12400
$Comp
L Device:C_Small C5
U 1 1 614E2697
P 7750 12500
F 0 "C5" H 7842 12546 50  0000 L CNN
F 1 "0.1u" H 7842 12455 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.18x1.45mm_HandSolder" H 7750 12500 50  0001 C CNN
F 3 "~" H 7750 12500 50  0001 C CNN
F 4 "C49678" H 7750 12500 50  0001 C CNN "JLC PCB Part Number"
	1    7750 12500
	1    0    0    -1  
$EndComp
Wire Wire Line
	7750 12300 7750 12400
$Comp
L Device:C_Small C6
U 1 1 61508FCA
P 8150 12500
F 0 "C6" H 8242 12546 50  0000 L CNN
F 1 "0.1u" H 8242 12455 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.18x1.45mm_HandSolder" H 8150 12500 50  0001 C CNN
F 3 "~" H 8150 12500 50  0001 C CNN
F 4 "C49678" H 8150 12500 50  0001 C CNN "JLC PCB Part Number"
	1    8150 12500
	1    0    0    -1  
$EndComp
Wire Wire Line
	8150 12300 8150 12400
$Comp
L power:VCC #PWR0103
U 1 1 615CE24B
P 6950 12200
F 0 "#PWR0103" H 6950 12050 50  0001 C CNN
F 1 "VCC" H 6965 12373 50  0000 C CNN
F 2 "" H 6950 12200 50  0001 C CNN
F 3 "" H 6950 12200 50  0001 C CNN
	1    6950 12200
	1    0    0    -1  
$EndComp
Wire Wire Line
	6950 12300 6950 12400
Connection ~ 7350 12300
Wire Wire Line
	7350 12300 6950 12300
Connection ~ 7750 12300
Wire Wire Line
	7750 12300 7350 12300
Wire Wire Line
	8150 12600 8150 12700
Wire Wire Line
	7350 12600 7350 12700
Connection ~ 7350 12700
Wire Wire Line
	7350 12700 6950 12700
Wire Wire Line
	7750 12600 7750 12700
Connection ~ 7750 12700
Wire Wire Line
	7750 12700 7350 12700
Wire Wire Line
	6950 12300 6950 12200
Connection ~ 6950 12300
$Comp
L keyboard_parts:SW_PUSH SW1
U 1 1 61774495
P 1800 12900
F 0 "SW1" H 1800 13155 50  0000 C CNN
F 1 "SW_PUSH" H 1800 13064 50  0000 C CNN
F 2 "Button_Switch_SMD:SW_SPST_TL3342" H 1800 12900 60  0001 C CNN
F 3 "" H 1800 12900 60  0000 C CNN
	1    1800 12900
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small_US R1
U 1 1 61775B2A
P 2100 12700
F 0 "R1" H 2168 12746 50  0000 L CNN
F 1 "10k" H 2168 12655 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" H 2100 12700 50  0001 C CNN
F 3 "~" H 2100 12700 50  0001 C CNN
F 4 "C17414" H 2100 12700 50  0001 C CNN "JLC PCB Part Number"
	1    2100 12700
	1    0    0    -1  
$EndComp
Wire Wire Line
	1500 12900 1500 13050
Wire Wire Line
	2100 12900 2100 12800
Wire Wire Line
	3600 12800 2450 12800
Wire Wire Line
	2450 12800 2450 12900
Wire Wire Line
	2450 12900 2100 12900
Connection ~ 2100 12900
Wire Wire Line
	8550 12300 8550 12400
Wire Wire Line
	8550 12600 8550 12700
$Comp
L Device:C_Small C8
U 1 1 61F52CBD
P 3050 12100
F 0 "C8" V 3188 12100 50  0000 C CNN
F 1 "1u" V 3293 12100 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.18x1.45mm_HandSolder" H 3050 12100 50  0001 C CNN
F 3 "~" H 3050 12100 50  0001 C CNN
F 4 "C28323" H 3050 12100 50  0001 C CNN "JLC PCB Part Number"
	1    3050 12100
	0    1    1    0   
$EndComp
Wire Wire Line
	2450 11900 2250 11900
Wire Wire Line
	2450 11800 2250 11800
Wire Wire Line
	2250 11500 2250 11600
$Comp
L Device:R_Small_US R4
U 1 1 61CF6E9E
P 2550 11900
F 0 "R4" V 2436 11959 50  0000 C CNN
F 1 "22" V 2436 11807 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" H 2550 11900 50  0001 C CNN
F 3 "~" H 2550 11900 50  0001 C CNN
F 4 "C17561" H 2550 11900 50  0001 C CNN "JLC PCB Part Number"
	1    2550 11900
	0    -1   -1   0   
$EndComp
$Comp
L Device:R_Small_US R3
U 1 1 61B2E09F
P 2550 11800
F 0 "R3" V 2436 11722 50  0000 C CNN
F 1 "22" V 2436 11859 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" H 2550 11800 50  0001 C CNN
F 3 "~" H 2550 11800 50  0001 C CNN
F 4 "C17561" H 2550 11800 50  0001 C CNN "JLC PCB Part Number"
	1    2550 11800
	0    1    1    0   
$EndComp
$Comp
L power:VCC #PWR0106
U 1 1 61C65223
P 2250 11500
F 0 "#PWR0106" H 2250 11350 50  0001 C CNN
F 1 "VCC" H 2265 11673 50  0000 C CNN
F 2 "" H 2250 11500 50  0001 C CNN
F 3 "" H 2250 11500 50  0001 C CNN
	1    2250 11500
	1    0    0    -1  
$EndComp
Wire Wire Line
	2250 12200 2250 12100
$Comp
L power:VCC #PWR0108
U 1 1 618205E7
P 2100 12600
F 0 "#PWR0108" H 2100 12450 50  0001 C CNN
F 1 "VCC" H 2115 12773 50  0000 C CNN
F 2 "" H 2100 12600 50  0001 C CNN
F 3 "" H 2100 12600 50  0001 C CNN
	1    2100 12600
	1    0    0    -1  
$EndComp
$Comp
L keyboard_parts:USB_mini_micro_B J1
U 1 1 000014A1
P 2100 11850
F 0 "J1" H 1982 12141 60  0000 C CNN
F 1 "USB_mini_micro_B" H 1950 12050 60  0001 C CNN
F 2 "keyboard_parts:USB_miniB_hirose_5S8" H 2050 11850 60  0001 C CNN
F 3 "" H 2050 11850 60  0000 C CNN
F 4 "C778749" H 2100 11850 50  0001 C CNN "JLC PCB Part Number"
	1    2100 11850
	1    0    0    -1  
$EndComp
Wire Wire Line
	2850 12000 2850 12100
Wire Wire Line
	2850 12100 2250 12100
Connection ~ 2250 12100
Wire Wire Line
	2950 12100 2850 12100
Connection ~ 2850 12100
Wire Wire Line
	2250 11600 2800 11600
Wire Wire Line
	2800 11600 2800 11700
Connection ~ 2250 11600
Wire Wire Line
	2250 11600 2250 11700
Wire Wire Line
	2800 11700 3600 11700
Wire Wire Line
	2650 11800 3600 11800
Wire Wire Line
	2650 11900 3600 11900
Wire Wire Line
	2850 12000 3600 12000
Wire Wire Line
	3150 12100 3600 12100
$Comp
L power:VCC #PWR0109
U 1 1 62503E33
P 5750 11600
F 0 "#PWR0109" H 5750 11450 50  0001 C CNN
F 1 "VCC" H 5765 11773 50  0000 C CNN
F 2 "" H 5750 11600 50  0001 C CNN
F 3 "" H 5750 11600 50  0001 C CNN
	1    5750 11600
	0    1    1    0   
$EndComp
$Comp
L power:VCC #PWR0110
U 1 1 62535085
P 5750 12600
F 0 "#PWR0110" H 5750 12450 50  0001 C CNN
F 1 "VCC" H 5765 12773 50  0000 C CNN
F 2 "" H 5750 12600 50  0001 C CNN
F 3 "" H 5750 12600 50  0001 C CNN
	1    5750 12600
	0    1    1    0   
$EndComp
$Comp
L power:VCC #PWR0111
U 1 1 62566344
P 5750 13600
F 0 "#PWR0111" H 5750 13450 50  0001 C CNN
F 1 "VCC" H 5765 13773 50  0000 C CNN
F 2 "" H 5750 13600 50  0001 C CNN
F 3 "" H 5750 13600 50  0001 C CNN
	1    5750 13600
	0    1    1    0   
$EndComp
$Comp
L power:VCC #PWR0112
U 1 1 625977C0
P 3600 12200
F 0 "#PWR0112" H 3600 12050 50  0001 C CNN
F 1 "VCC" H 3615 12373 50  0001 C CNN
F 2 "" H 3600 12200 50  0001 C CNN
F 3 "" H 3600 12200 50  0001 C CNN
	1    3600 12200
	0    -1   -1   0   
$EndComp
$Comp
L power:VCC #PWR0113
U 1 1 625C8985
P 3600 12900
F 0 "#PWR0113" H 3600 12750 50  0001 C CNN
F 1 "VCC" H 3615 13073 50  0001 C CNN
F 2 "" H 3600 12900 50  0001 C CNN
F 3 "" H 3600 12900 50  0001 C CNN
	1    3600 12900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7750 12300 8150 12300
Connection ~ 8150 12300
Wire Wire Line
	8150 12300 8550 12300
Wire Wire Line
	7750 12700 8150 12700
Connection ~ 8150 12700
Wire Wire Line
	8150 12700 8550 12700
NoConn ~ 5750 11800
NoConn ~ 2250 12000
Wire Wire Line
	6500 12700 6350 12700
Wire Wire Line
	6500 12800 6500 12700
$Comp
L Device:R_Small_US R2
U 1 1 6194F3F6
P 6250 12700
F 0 "R2" V 6455 12700 50  0000 C CNN
F 1 "10k" V 6364 12700 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" H 6250 12700 50  0001 C CNN
F 3 "~" H 6250 12700 50  0001 C CNN
F 4 "C17414" H 6250 12700 50  0001 C CNN "JLC PCB Part Number"
	1    6250 12700
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6150 12700 5750 12700
$Comp
L Device:D D_F13
U 1 1 00000390
P 6900 6900
F 0 "D_F13" V 6946 6821 50  0000 R CNN
F 1 "D" V 6845 6821 50  0000 R CNN
F 2 "keyboard_parts:D_SOD123_axial" H 6900 6900 50  0001 C CNN
F 3 "~" H 6900 6900 50  0001 C CNN
F 4 "C81598" H 6900 6900 50  0001 C CNN "JLC PCB Part Number"
	1    6900 6900
	0    -1   -1   0   
$EndComp
$Comp
L keyboard_parts:KEYSW K_F13
U 1 1 00000391
P 7250 6650
F 0 "K_F13" H 7250 6883 60  0000 C CNN
F 1 "KEYSW" H 7250 6550 60  0001 C CNN
F 2 "keyswitches:Kailh_socket_MX_1u" H 7250 6650 60  0001 C CNN
F 3 "" H 7250 6650 60  0000 C CNN
	1    7250 6650
	1    0    0    -1  
$EndComp
Wire Wire Line
	6950 12600 6950 12700
$Comp
L Device:C_Small C3
U 1 1 6146F89A
P 6950 12500
F 0 "C3" H 7042 12546 50  0000 L CNN
F 1 "0.1u" H 7042 12455 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.18x1.45mm_HandSolder" H 6950 12500 50  0001 C CNN
F 3 "~" H 6950 12500 50  0001 C CNN
F 4 "C49678" H 6950 12500 50  0001 C CNN "JLC PCB Part Number"
	1    6950 12500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0102
U 1 1 611F3106
P 2250 12200
F 0 "#PWR0102" H 2250 11950 50  0001 C CNN
F 1 "GND" H 2255 12027 50  0000 C CNN
F 2 "" H 2250 12200 50  0001 C CNN
F 3 "" H 2250 12200 50  0001 C CNN
	1    2250 12200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0104
U 1 1 612B7770
P 1500 13050
F 0 "#PWR0104" H 1500 12800 50  0001 C CNN
F 1 "GND" H 1505 12877 50  0000 C CNN
F 2 "" H 1500 13050 50  0001 C CNN
F 3 "" H 1500 13050 50  0001 C CNN
	1    1500 13050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0105
U 1 1 6131A257
P 2300 14000
F 0 "#PWR0105" H 2300 13750 50  0001 C CNN
F 1 "GND" H 2305 13827 50  0000 C CNN
F 2 "" H 2300 14000 50  0001 C CNN
F 3 "" H 2300 14000 50  0001 C CNN
	1    2300 14000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0107
U 1 1 613AEDEB
P 3600 13000
F 0 "#PWR0107" H 3600 12750 50  0001 C CNN
F 1 "GND" H 3605 12827 50  0000 C CNN
F 2 "" H 3600 13000 50  0001 C CNN
F 3 "" H 3600 13000 50  0001 C CNN
	1    3600 13000
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0114
U 1 1 614A6CB5
P 5750 13700
F 0 "#PWR0114" H 5750 13450 50  0001 C CNN
F 1 "GND" H 5755 13527 50  0000 C CNN
F 2 "" H 5750 13700 50  0001 C CNN
F 3 "" H 5750 13700 50  0001 C CNN
	1    5750 13700
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0115
U 1 1 6153B708
P 6500 12800
F 0 "#PWR0115" H 6500 12550 50  0001 C CNN
F 1 "GND" H 6505 12627 50  0000 C CNN
F 2 "" H 6500 12800 50  0001 C CNN
F 3 "" H 6500 12800 50  0001 C CNN
	1    6500 12800
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0116
U 1 1 6159E725
P 5750 12500
F 0 "#PWR0116" H 5750 12250 50  0001 C CNN
F 1 "GND" H 5755 12327 50  0000 C CNN
F 2 "" H 5750 12500 50  0001 C CNN
F 3 "" H 5750 12500 50  0001 C CNN
	1    5750 12500
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0117
U 1 1 6163285D
P 5750 11700
F 0 "#PWR0117" H 5750 11450 50  0001 C CNN
F 1 "GND" H 5755 11527 50  0000 C CNN
F 2 "" H 5750 11700 50  0001 C CNN
F 3 "" H 5750 11700 50  0001 C CNN
	1    5750 11700
	0    -1   -1   0   
$EndComp
Connection ~ 6950 12700
Wire Wire Line
	6950 12700 6950 12800
Wire Wire Line
	4900 9050 7900 9050
Connection ~ 7900 9050
Wire Wire Line
	7900 9050 11900 9050
$Comp
L Device:C_Small C7
U 1 1 61FB0834
P 8550 12500
F 0 "C7" H 8642 12546 50  0000 L CNN
F 1 "4.7u" H 8642 12455 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.18x1.45mm_HandSolder" H 8550 12500 50  0001 C CNN
F 3 "~" H 8550 12500 50  0001 C CNN
F 4 "C1779" H 8550 12500 50  0001 C CNN "JLC PCB Part Number"
	1    8550 12500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0101
U 1 1 611C1FD3
P 6950 12800
F 0 "#PWR0101" H 6950 12550 50  0001 C CNN
F 1 "GND" H 6955 12627 50  0000 C CNN
F 2 "" H 6950 12800 50  0001 C CNN
F 3 "" H 6950 12800 50  0001 C CNN
	1    6950 12800
	1    0    0    -1  
$EndComp
Text GLabel 3600 12500 0    50   Input ~ 0
row1
$Comp
L keyboard_parts:ATMEGA32U4 U1
U 1 1 000021A1
P 4650 12650
F 0 "U1" H 4675 13987 60  0000 C CNN
F 1 "ATMEGA32U4" H 4675 13881 60  0000 C CNN
F 2 "Package_QFP:TQFP-44_10x10mm_P0.8mm" H 4650 12650 60  0001 C CNN
F 3 "" H 4650 12650 60  0000 C CNN
F 4 "C44854" H 4650 12650 50  0001 C CNN "JLC PCB Part Number"
	1    4650 12650
	1    0    0    -1  
$EndComp
$Comp
L keyboard_parts:XTAL_GND X1
U 1 1 612F987B
P 2300 13550
F 0 "X1" H 2300 13842 60  0000 C CNN
F 1 "XTAL_GND" H 2300 13736 60  0000 C CNN
F 2 "Crystal:Crystal_SMD_SeikoEpson_FA238-4Pin_3.2x2.5mm" H 2300 13550 60  0001 C CNN
F 3 "" H 2300 13550 60  0000 C CNN
F 4 "C841681" H 2300 13550 50  0001 C CNN "JLC PCB Part Number"
	1    2300 13550
	1    0    0    -1  
$EndComp
Connection ~ 2250 12200
Wire Notes Line style solid
	20050 1950 1050 1950
Wire Notes Line style solid
	1050 1950 1050 9450
Wire Notes Line style solid
	1050 9450 20050 9450
Wire Notes Line style solid
	20050 9450 20050 1950
Wire Notes Line style solid
	1000 10900 9150 10900
Wire Notes Line style solid
	9150 10900 9150 14500
Wire Notes Line style solid
	9150 14500 1000 14500
Wire Notes Line style solid
	1000 14500 1000 10900
Text Notes 950  10700 0    276  ~ 55
Microcontroller Assembly
Text Notes 1050 1650 0    276  ~ 55
Switch Matrix
$EndSCHEMATC
